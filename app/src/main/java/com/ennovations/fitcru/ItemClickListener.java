package com.ennovations.fitcru;

public interface ItemClickListener {

    void onAdapterItemClickListener(int id);
}

package com.ennovations.fitcru;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.ennovations.fitcru.orderFood.OrderFoodActivity;
import com.ennovations.fitcru.pojo.DataClass;
import com.ennovations.fitcru.adapter.HorizonDataAdapter;
import com.ennovations.fitcru.workout.WorkoutActivity;
import com.facebook.login.Login;
import com.google.android.material.bottomnavigation.BottomNavigationView;
//import com.google.android.material.floatingactionbutton.FloatingActionButton;
//import com.mikhaellopez.circularprogressbar.CircularProgressBar;


public class DashboardActivity extends AppCompatActivity {
    RecyclerView rv;
    RecyclerView.LayoutManager layoutManager;
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        //progressBar();
        bottomNav();


        Toolbar toolbar=findViewById(R.id.toolbar33);
        setSupportActionBar(toolbar);
        ImageView rrr= findViewById(R.id.rrr);


         rv =findViewById(R.id.rv);

         rv.setLayoutManager(new LinearLayoutManager(this,  LinearLayoutManager.HORIZONTAL, false));

         rv.setAdapter(new HorizonDataAdapter(getList()));

        rrr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DashboardActivity.this, "clicked", Toast.LENGTH_SHORT).show();
            }
        });


    }


    private DataClass[] getList() {
        DataClass[] myListData = new DataClass[] {
                new DataClass(R.drawable.ic_hire_trainer,"Hire Personal Trainer"),
                new DataClass(R.drawable.ic_lab_test,"Book A Lab Test"),
                new DataClass(R.drawable.ic_see_video,"See Fitness Videos"),
                new DataClass(R.drawable.ic_mediation,"Guided Meditation"),
                new DataClass(R.drawable.ic_online_therapy,"Online Therapy"),
        };
        return myListData;
    }


    private void bottomNav() {
        //Bottom Nav bar
        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setBackground(null);
        bottomNavigationView.getMenu().getItem(0).setCheckable(false);
        bottomNavigationView.getMenu().getItem(1).setCheckable(false);
        bottomNavigationView.getMenu().getItem(2).setCheckable(false);
        bottomNavigationView.getMenu().getItem(3).setCheckable(false);
        bottomNavigationView.getMenu().getItem(4).setCheckable(false);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId())
                {
                    case R.id.nav_workout: startActivity(new Intent(DashboardActivity.this, WorkoutActivity.class));
                        break;
                    case R.id.nav_Nutrition:startActivity(new Intent(DashboardActivity.this, MainActivity.class));
                        break;
                    case R.id.nav_coaching:
                        break;
                    case R.id.nav_order:startActivity(new Intent(DashboardActivity.this, OrderFoodActivity.class));
                }
                return true;
            }
        });
    }


/*
    private void progressBar() {
        ////Progress bar 1
        CircularProgressBar circularProgressBar = findViewById(R.id.progress1);
// Set Progress
        circularProgressBar.setProgress(85f);
// or with animation
        circularProgressBar.setProgressWithAnimation(85f, 1000L); // =1s
// Set Progress Max
        circularProgressBar.setProgressMax(100f);
// Set ProgressBar Color
        circularProgressBar.setProgressBarColor(Color.parseColor("#ed2b2b"));
// Set background ProgressBar Color
        circularProgressBar.setBackgroundProgressBarColor(Color.parseColor("#ebebeb"));
// Set Width
        circularProgressBar.setProgressBarWidth(15f); // in DP
        circularProgressBar.setBackgroundProgressBarWidth(15f); // in DP
// Other
        circularProgressBar.setRoundBorder(true);
        // circularProgressBar.setStartAngle(180f);
        circularProgressBar.setProgressDirection(CircularProgressBar.ProgressDirection.TO_RIGHT);

////Progress bar 2
        CircularProgressBar circularProgressBar2 = findViewById(R.id.progress2);
// Set Progress
        circularProgressBar2.setProgress(75f);
// or with animation
        circularProgressBar2.setProgressWithAnimation(75f, 1000L); // =1s
// Set Progress Max
        circularProgressBar2.setProgressMax(100f);
// Set ProgressBar Color
        circularProgressBar2.setProgressBarColor(Color.parseColor("#00cf0a"));
// Set background ProgressBar Color
        circularProgressBar2.setBackgroundProgressBarColor(Color.parseColor("#ebebeb"));
// Set Width
        circularProgressBar2.setProgressBarWidth(15f); // in DP
        circularProgressBar2.setBackgroundProgressBarWidth(15f); // in DP
// Other
        circularProgressBar2.setRoundBorder(true);
        // circularProgressBar.setStartAngle(180f);
        circularProgressBar2.setProgressDirection(CircularProgressBar.ProgressDirection.TO_RIGHT);


        ////Progress bar 3
        CircularProgressBar circularProgressBar3 = findViewById(R.id.progress3);
// Set Progress
        circularProgressBar3.setProgress(60f);
// or with animation
        circularProgressBar3.setProgressWithAnimation(60f, 1000L); // =1s
// Set Progress Max
        circularProgressBar3.setProgressMax(100f);
// Set ProgressBar Color
        circularProgressBar3.setProgressBarColor(Color.parseColor("#0ad2ff"));
// Set background ProgressBar Color
        circularProgressBar3.setBackgroundProgressBarColor(Color.parseColor("#ebebeb"));
// Set Width
        circularProgressBar3.setProgressBarWidth(15f); // in DP
        circularProgressBar3.setBackgroundProgressBarWidth(15f); // in DP
// Other
        circularProgressBar3.setRoundBorder(true);
        // circularProgressBar.setStartAngle(180f);
        circularProgressBar3.setProgressDirection(CircularProgressBar.ProgressDirection.TO_RIGHT);


        ////Progress bar 4
        CircularProgressBar circularProgressBar4 = findViewById(R.id.progress4);
// Set Progress
        circularProgressBar4.setProgress(50f);
// or with animation
        circularProgressBar4.setProgressWithAnimation(50f, 1000L); // =1s
// Set Progress Max
        circularProgressBar4.setProgressMax(100f);
// Set ProgressBar Color
        circularProgressBar4.setProgressBarColor(Color.BLACK);
// Set background ProgressBar Color
        circularProgressBar4.setBackgroundProgressBarColor(Color.parseColor("#ebebeb"));
// Set Width
        circularProgressBar4.setProgressBarWidth(15f); // in DP
        circularProgressBar4.setBackgroundProgressBarWidth(15f); // in DP
// Other
        circularProgressBar4.setRoundBorder(true);
        // circularProgressBar.setStartAngle(180f);
        circularProgressBar4.setProgressDirection(CircularProgressBar.ProgressDirection.TO_RIGHT);

    }*/

}
package com.ennovations.fitcru;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.ennovations.fitcru.workout.SelectStudioActivity;

public class ForgetPassActivity extends AppCompatActivity {

    EditText email;
    String _email;
    TextView reset_pass, text_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pass);

        email = (EditText) findViewById(R.id.edit_email);
        reset_pass = (TextView) findViewById(R.id.reset_password);
        text_back = (TextView) findViewById(R.id.text_back);

        reset_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _email = email.getText().toString();

                final Dialog dialog = new Dialog(getApplicationContext());
                dialog.setContentView(R.layout.dialog_forget);
                dialog.setTitle("Custom Dialog");

                TextView back_sign = (TextView) dialog.findViewById(R.id.back_signin);

                dialog.show();

                back_sign.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(getApplicationContext(), EmailLoginActivity.class);
                        startActivity(intent);

                    }

                });
            }
        });

        text_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ForgetPassActivity.this,EmailLoginActivity.class);
                startActivity(intent);
            }
        });

    }
}
package com.ennovations.fitcru;

import static com.ennovations.fitcru.util.Common.device_name;
import static com.ennovations.fitcru.util.Common.hasInternetConnection;
import static com.ennovations.fitcru.util.Common.setToken;
import static com.ennovations.fitcru.util.Common.toast;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;

import com.ennovations.fitcru.Api.ApiClient;
import com.ennovations.fitcru.Api.MyApiEndpointInterface;
import com.ennovations.fitcru.pojo.CommonResponse;
import com.ennovations.fitcru.pojo.SocialAuthBody;
import com.ennovations.fitcru.pojo.SocialAuthResponse;
import com.ennovations.fitcru.util.Common;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUPActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final int RC_SIGN_IN = 9001;
    private static final String EMAIL = "email";
    EditText mobile_number;
    String _contact, _code, _number;
    TextView sign_up, privacy, terms;
    ImageView image_google, image_facebook, image_email;
    GoogleApiClient mGoogleApiClient;
    CallbackManager callbackManager;
    LoginButton loginButton;
    MyApiEndpointInterface myApiEndpointInterface;
    ProgressBar pb;
    AppCompatSpinner sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_upactivity);

        mobile_number = (EditText) findViewById(R.id.mobile_number);
        sign_up = (TextView) findViewById(R.id.text_signup);
        privacy = (TextView) findViewById(R.id.text_privacy);
        terms = (TextView) findViewById(R.id.text_terms);
        image_google = (ImageView) findViewById(R.id.image_google);
        image_facebook = (ImageView) findViewById(R.id.image_facebook);
        image_email = (ImageView) findViewById(R.id.image_email);
        pb = findViewById(R.id.progressBarr);
        loginButton = (LoginButton) findViewById(R.id.login_button);
        sp = findViewById(R.id.spinner);

        callbackManager = CallbackManager.Factory.create();

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.country_code));
        sp.setAdapter(adapter);

        myApiEndpointInterface = ApiClient.getClient().create(MyApiEndpointInterface.class);


//        privacy.setPaintFlags(privacy.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_clientid))
                .requestProfile()
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        image_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SignUPActivity.this, EmailSignUpActivity.class);
                startActivity(intent);
                finish();
            }
        });


        sign_up.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {

                _contact = mobile_number.getText().toString();
                _number = sp.getSelectedItem().toString() + _contact;

                if (mobile_number.getText().toString().isEmpty()) {
                    Common.toast(SignUPActivity.this, "Please enter mobile number");
                } else if (mobile_number.getText().length() != 10) {
                    Common.toast(SignUPActivity.this, "Please enter a valid mobile number");
                } else {
                    pb.setVisibility(View.VISIBLE);
                    _contact = mobile_number.getText().toString();
                    _number = _code + _contact;
                    if (hasInternetConnection(SignUPActivity.this)){
                        callSendOtpApi(_number);
                    }else {
                        pb.setVisibility(View.GONE);
                        toast(SignUPActivity.this,"Please check your internet connection");
                    }
                }
            }
        });

        image_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pb.setVisibility(View.VISIBLE);
                signInClick();

            }
        });

        image_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pb.setVisibility(View.VISIBLE);
                loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));
                loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        pb.setVisibility(View.GONE);
                        // App code
                        //loginResult.getAccessToken();
                        //loginResult.getRecentlyDeniedPermissions()
                        //loginResult.getRecentlyGrantedPermissions()
                        boolean loggedIn = AccessToken.getCurrentAccessToken() == null;
                        Log.d("API123", loggedIn + " ??");
                    }

                    @Override
                    public void onCancel() {
                        pb.setVisibility(View.GONE);
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Common.toast(SignUPActivity.this, exception.getLocalizedMessage());
                        pb.setVisibility(View.GONE);
                        // App code
                    }
                });
            }
        });
    }

    private void callSendOtpApi(String number) {

        Call<CommonResponse> call1 = myApiEndpointInterface.sendOtp(number);

        call1.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                CommonResponse commonResponse = response.body();
                pb.setVisibility(View.GONE);
                Common.toast(SignUPActivity.this, commonResponse.getMessage());
                if (commonResponse.getMessage().equals("success") && commonResponse.getStatus() == 200) {
                    Intent i = new Intent(SignUPActivity.this, VerificationActivity.class);
                    i.putExtra("mobile_number", _number);
                    startActivity(i);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                pb.setVisibility(View.GONE);
                call.cancel();
                Common.toast(SignUPActivity.this, t.getLocalizedMessage());

            }
        });
    }


    private void signInClick() {
        //pb.setVisibility(View.GONE);
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            pb.setVisibility(View.GONE);

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

    }

    private void callSocialAuthApi(GoogleSignInResult result) {

        SocialAuthBody socialAuthBody = new SocialAuthBody();

        socialAuthBody.device_name = device_name();
        socialAuthBody.name = result.getSignInAccount().getDisplayName();
        socialAuthBody.provider_id = result.getSignInAccount().getId();
        socialAuthBody.provider_name = result.getSignInAccount().getGivenName();

        myApiEndpointInterface.getUserDetails(socialAuthBody).enqueue(new Callback<SocialAuthResponse>() {
            @Override
            public void onResponse(Call<SocialAuthResponse> call, Response<SocialAuthResponse> response) {
                pb.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.isSuccessful() && response.body().status == 200) {
                        setToken(SignUPActivity.this,response.body().token);
                        Common.toast(SignUPActivity.this, response.body().message);
                    } else {
                        Common.toast(SignUPActivity.this, response.body().message);
                    }
                } else {
                    Common.toast(SignUPActivity.this, response.body().message);
                }
            }

            @Override
            public void onFailure(Call<SocialAuthResponse> call, Throwable t) {
                pb.setVisibility(View.GONE);
                Common.toast(SignUPActivity.this, t.getLocalizedMessage());
            }
        });
    }

    private void getUserProfile(AccessToken currentAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                currentAccessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.d("TAG", object.toString());
                        try {
                            String first_name = object.getString("first_name");
                            String last_name = object.getString("last_name");
                            String email = object.getString("email");
                            String id = object.getString("id");
                            String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";

//                            txtUsername.setText("First Name: " + first_name + "\nLast Name: " + last_name);
//                            txtEmail.setText(email);
//                            Picasso.with(EmailLoginActivity.this).load(image_url).into(imageView);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();

    }


    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {
            Common.toast(SignUPActivity.this, result.getSignInAccount().getEmail());
            result.getSignInAccount().getEmail();
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String idToken = acct.getIdToken();
            Log.d("DD", idToken);

            pb.setVisibility(View.VISIBLE);
            callSocialAuthApi(result);
//            signout.setVisibility(View.VISIBLE);
//            signin.setVisibility(View.GONE);

        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Common.toast(SignUPActivity.this, connectionResult.getErrorMessage());
    }
}
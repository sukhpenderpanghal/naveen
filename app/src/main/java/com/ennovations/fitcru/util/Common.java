package com.ennovations.fitcru.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Patterns;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.ennovations.fitcru.EmailLoginActivity;
import com.ennovations.fitcru.WelcomeActivity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Common {

    public static void toast(Context mContext,String message){
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }

    public static String device_name() {
        return Build.MANUFACTURER
                + " " + Build.MODEL + " " + Build.VERSION.RELEASE
                + " " + Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();

    }

    public static boolean validEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public static boolean isValidPassword(final String password) {
        String PASSWORD_PATTERN =
                "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,20}$";

        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);

        Matcher matcher = pattern.matcher(password);
        return matcher.matches();

    }

    public static String getToken(Context context){
        SharedPreferences prefs = context.getSharedPreferences("UserToken", Context.MODE_PRIVATE);
        String token = prefs.getString("token", "");
        return token;
    }

    public  static void setToken(Context context,String token){
        SharedPreferences.Editor editor = context.getSharedPreferences("UserToken", Context.MODE_PRIVATE).edit();
        editor.putString("token", token);
        editor.apply();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static boolean hasInternetConnection(Context context) {
        final ConnectivityManager connectivityManager = (ConnectivityManager)context.
                getSystemService(Context.CONNECTIVITY_SERVICE);

        final Network network = connectivityManager.getActiveNetwork();
        final NetworkCapabilities capabilities = connectivityManager
                .getNetworkCapabilities(network);

        return capabilities != null
                && capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED);
    }
}

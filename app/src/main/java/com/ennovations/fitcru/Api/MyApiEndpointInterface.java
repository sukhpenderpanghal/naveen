package com.ennovations.fitcru.Api;

import com.ennovations.fitcru.pojo.CommonResponse;
import com.ennovations.fitcru.pojo.EmailAuthenticationResponse;
import com.ennovations.fitcru.pojo.EmailSignInResponse;
import com.ennovations.fitcru.pojo.SingleStudioResponse;
import com.ennovations.fitcru.pojo.SocialAuthBody;
import com.ennovations.fitcru.pojo.SocialAuthResponse;
import com.ennovations.fitcru.pojo.StudioId;
import com.ennovations.fitcru.pojo.StudioResponsePojo;
import com.ennovations.fitcru.pojo.TodaySessionResponse;
import com.ennovations.fitcru.pojo.UpDateClientDataPojo;
import com.ennovations.fitcru.pojo.UpdateClientResponse;
import com.ennovations.fitcru.pojo.WorkOutChallengeResponse;
import com.ennovations.fitcru.pojo.WorkOutPlanDetailsPojo;
import com.ennovations.fitcru.pojo.WorkOutPlansPojo;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface MyApiEndpointInterface {

    @POST("register")
    @FormUrlEncoded
    Call<EmailSignInResponse> setPostRegister(@Field("email") String email,
                                              @Field("password") String password,
                                              @Field("device_name") String device_name);

    @POST("sanctum/token")
    @FormUrlEncoded
    Call<EmailAuthenticationResponse> emailAuthentication(@Field("email") String email,
                                                          @Field("password") String password,
                                                          @Field("device_name") String device_name);

    /* send otp */
    @POST("sendotp")
    @FormUrlEncoded
    Call<CommonResponse> sendOtp(@Field("mobile") String mobile);

    /* verify otp */
    @POST("verifyotp")
    @FormUrlEncoded
    Call<EmailSignInResponse> verifyOtp(@Field("otp") String otp,
                                        @Field("device_name") String device_name,
                                        @Field("mobile") String mobile);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @PUT("updateclient")
    Call<UpdateClientResponse> upDateClient(@Header("Authorization") String token,
                                            @Body UpDateClientDataPojo upDateClientDataPojo);

    /* WorkOut Plan List */
    @GET("planlist")
    Call<WorkOutPlansPojo> getWorkOutPlans();

    /* WorkOut Particular Plan Details */
    @POST("plan")
    @FormUrlEncoded
    Call<WorkOutPlanDetailsPojo> getParticularPlanDetails(@Field("id") int id);

    /* get Studio WorkOutList */
    @GET("master/studio")
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    Call<StudioResponsePojo> getStudiosList(@Header("Authorization") String token);

    /* get particular studio */
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("getstudio")
    Call<SingleStudioResponse> getParticularStudio(@Header("Authorization") String token,
                                                   @Body StudioId jsonObject);

    /* get workout challenges */
    @POST("getchallenge")
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    Call<WorkOutChallengeResponse> getWorkOutChallenges(@Header("Authorization") String token);

    /* get today session of user */
    @POST("todaysession")
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    Call<TodaySessionResponse> getTodaySession(@Header("Authorization") String token);

    /* social auth logins */
    @POST("socialauth")
    Call<SocialAuthResponse> getUserDetails(@Body SocialAuthBody socialAuthBody);

}

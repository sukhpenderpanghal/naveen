package com.ennovations.fitcru;

import static com.ennovations.fitcru.util.Common.device_name;
import static com.ennovations.fitcru.util.Common.setToken;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.ennovations.fitcru.Api.ApiClient;
import com.ennovations.fitcru.Api.MyApiEndpointInterface;
import com.ennovations.fitcru.pojo.EmailSignInResponse;
import com.ennovations.fitcru.pojo.SocialAuthBody;
import com.ennovations.fitcru.pojo.SocialAuthResponse;
import com.ennovations.fitcru.util.Common;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EmailSignUpActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final int RC_SIGN_IN = 9001;
    private static final String EMAIL = "email";
    EditText email, password;
    String _email, _password, deviceName;
    ImageView image_google, image_facebook, image_email;
    TextView sign_up;
    GoogleApiClient mGoogleApiClient;
    CallbackManager callbackManager;
    LoginButton loginButton;
    MyApiEndpointInterface myApiEndpointInterface;
    ProgressBar pb;

    public static boolean isValid(final String password) {
        String PASSWORD_PATTERN =
                "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,20}$";

        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);

        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_sign_up);

        email = findViewById(R.id.edit_email);
        password = findViewById(R.id.edit_password);
        sign_up = findViewById(R.id.text_signup);
        image_google = findViewById(R.id.image_google);
        image_facebook = findViewById(R.id.image_facebook);
        image_email = findViewById(R.id.image_email);
        pb = findViewById(R.id.progressBarr);

        onClickListener();
        myApiEndpointInterface = ApiClient.getClient().create(MyApiEndpointInterface.class);
        deviceName = Common.device_name();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_clientid))
                .requestProfile()
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        image_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(EmailSignUpActivity.this, SignUPActivity.class);
                startActivity(intent);
                finish();
            }
        });


        image_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInClick();

            }
        });

        image_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));
                callbackManager = CallbackManager.Factory.create();
                loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        //loginResult.getAccessToken();
                        //loginResult.getRecentlyDeniedPermissions()
                        //loginResult.getRecentlyGrantedPermissions()
                        boolean loggedIn = AccessToken.getCurrentAccessToken() == null;
                        Log.d("API123", loggedIn + " ??");

                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
            }
        });


    }

    private void callSignUpEmailApi(String email, String password, String device_name) {

        Call<EmailSignInResponse> call1 = myApiEndpointInterface.setPostRegister(email, password, device_name);
        call1.enqueue(new Callback<EmailSignInResponse>() {
            @Override
            public void onResponse(Call<EmailSignInResponse> call, Response<EmailSignInResponse> response) {
                EmailSignInResponse emailSignInDataClass = response.body();
                pb.setVisibility(View.GONE);
                Common.toast(EmailSignUpActivity.this, emailSignInDataClass.message);
                if (emailSignInDataClass.message.equals("success") && emailSignInDataClass.status == 200) {
                    startActivity(new Intent(EmailSignUpActivity.this, WelcomeActivity.class));
                    finish();
                }
            }

            @Override
            public void onFailure(Call<EmailSignInResponse> call, Throwable t) {
                pb.setVisibility(View.GONE);
                call.cancel();
                Common.toast(EmailSignUpActivity.this, call.toString());
            }
        });
    }

    private void signInClick() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public boolean validEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

//        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);

        }

    }

    private void getUserProfile(AccessToken currentAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                currentAccessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.d("TAG", object.toString());
                        try {
                            String first_name = object.getString("first_name");
                            String last_name = object.getString("last_name");
                            String email = object.getString("email");
                            String id = object.getString("id");
                            String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";

//                            txtUsername.setText("First Name: " + first_name + "\nLast Name: " + last_name);
//                            txtEmail.setText(email);
//                            Picasso.with(EmailLoginActivity.this).load(image_url).into(imageView);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();

    }


    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String idToken = acct.getIdToken();
            Log.d("DD", idToken);
//            signout.setVisibility(View.VISIBLE);
//            signin.setVisibility(View.GONE);

            pb.setVisibility(View.VISIBLE);
            callSocialAuthApi(result);
        }
    }

    private void callSocialAuthApi(GoogleSignInResult result) {

        SocialAuthBody socialAuthBody = new SocialAuthBody();

        socialAuthBody.device_name = device_name();
        socialAuthBody.name = result.getSignInAccount().getDisplayName();
        socialAuthBody.provider_id = result.getSignInAccount().getId();
        socialAuthBody.provider_name = result.getSignInAccount().getGivenName();

        myApiEndpointInterface.getUserDetails(socialAuthBody).enqueue(new Callback<SocialAuthResponse>() {
            @Override
            public void onResponse(Call<SocialAuthResponse> call, Response<SocialAuthResponse> response) {
                pb.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.isSuccessful() && response.body().status == 200) {
                        setToken(EmailSignUpActivity.this, response.body().token);
                        Common.toast(EmailSignUpActivity.this, response.body().message);
                    } else {
                        Common.toast(EmailSignUpActivity.this, response.body().message);
                    }
                } else {
                    Common.toast(EmailSignUpActivity.this, response.body().message);
                }
            }

            @Override
            public void onFailure(Call<SocialAuthResponse> call, Throwable t) {
                pb.setVisibility(View.GONE);
                Common.toast(EmailSignUpActivity.this, t.getLocalizedMessage());
            }
        });
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void onClickListener() {
        sign_up.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.text_signup: {
                _email = email.getText().toString();
                _password = password.getText().toString();
                if (!validEmail(_email)) {
                    email.setError("You Email does not match the pattern");
                } else if (!isValid(_password)) {
                    password.setError("You Password does not match the pattern");
                } else {
                    pb.setVisibility(View.VISIBLE);
                    callSignUpEmailApi(_email, _password, deviceName);
                }
            }
        }
    }
}

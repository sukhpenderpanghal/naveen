package com.ennovations.fitcru;

import static com.ennovations.fitcru.util.Common.device_name;
import static com.ennovations.fitcru.util.Common.setToken;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.ennovations.fitcru.Api.ApiClient;
import com.ennovations.fitcru.Api.MyApiEndpointInterface;
import com.ennovations.fitcru.pojo.EmailAuthenticationResponse;
import com.ennovations.fitcru.pojo.SocialAuthBody;
import com.ennovations.fitcru.pojo.SocialAuthResponse;
import com.ennovations.fitcru.util.Common;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmailLoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int RC_SIGN_IN = 9001;
    EditText email, password;
    String _email, _password, deviceName;
    ImageView image_google, image_facebook, image_email;
    TextView sign_in, text_forget;
    CallbackManager callbackManager;
    LoginButton loginButton;
    MyApiEndpointInterface myApiEndpointInterface;
    ProgressBar pb;

    public static boolean isValid(final String password) {
        String PASSWORD_PATTERN =
                "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,20}$";

        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);

        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_login);

        email = findViewById(R.id.edit_email);
        password = findViewById(R.id.edit_password);
        sign_in = findViewById(R.id.text_signin);
        image_google = findViewById(R.id.image_google);
        image_facebook = findViewById(R.id.image_facebook);
        image_email = findViewById(R.id.image_email);
        text_forget = findViewById(R.id.text_forget);
        loginButton = findViewById(R.id.login_button);
        pb = findViewById(R.id.progressBarr);

        myApiEndpointInterface = ApiClient.getClient().create(MyApiEndpointInterface.class);
        deviceName = Common.device_name();

        onClickListener();
        FacebookSdk.sdkInitialize(getApplicationContext());
        SpannableString content = new SpannableString("Forgot Password?");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        text_forget.setText(content);

       /* boolean loggedOut = AccessToken.getCurrentAccessToken() == null;
        if (!loggedOut) {
//            Picasso.with(this).load(Profile.getCurrentProfile().getProfilePictureUri(200, 200)).into(imageView);
//            Log.d("TAG", "Username is: " + Profile.getCurrentProfile().getName());

            //Using Graph API
            getUserProfile(AccessToken.getCurrentAccessToken());
        }*/

        image_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));
                callbackManager = CallbackManager.Factory.create();
                loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        boolean loggedIn = AccessToken.getCurrentAccessToken() == null;
                        Log.d("API123", loggedIn + " ??");

                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
            }
        });

        image_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EmailLoginActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        text_forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EmailLoginActivity.this, ForgetPassActivity.class);
                startActivity(intent);
            }
        });

    }

    private void callSignInEmailAuthenticateApi(String email, String password, String device_name) {

        Call<EmailAuthenticationResponse> call1 = myApiEndpointInterface.emailAuthentication(email, password, device_name);
        call1.enqueue(new Callback<EmailAuthenticationResponse>() {
            @Override
            public void onResponse(Call<EmailAuthenticationResponse> call, Response<EmailAuthenticationResponse> response) {
                EmailAuthenticationResponse emailAuthenticationResponse = response.body();
                pb.setVisibility(View.GONE);
                Common.toast(EmailLoginActivity.this, emailAuthenticationResponse.message);
                if (emailAuthenticationResponse.message.equals("success") && emailAuthenticationResponse.status == 200) {
                    startActivity(new Intent(EmailLoginActivity.this, WelcomeActivity.class));
                    finish();
                }
            }

            @Override
            public void onFailure(Call<EmailAuthenticationResponse> call, Throwable t) {
                pb.setVisibility(View.GONE);
                call.cancel();
                Common.toast(EmailLoginActivity.this, call.toString());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            pb.setVisibility(View.GONE);

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {
            Common.toast(EmailLoginActivity.this, result.getSignInAccount().getEmail());
            result.getSignInAccount().getEmail();
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String idToken = acct.getIdToken();
            Log.d("DD", idToken);
//            signout.setVisibility(View.VISIBLE);
//            signin.setVisibility(View.GONE);
            pb.setVisibility(View.VISIBLE);
            callSocialAuthApi(result);
        }
    }

    private void callSocialAuthApi(GoogleSignInResult result) {

        SocialAuthBody socialAuthBody = new SocialAuthBody();

        socialAuthBody.device_name = device_name();
        socialAuthBody.name = result.getSignInAccount().getDisplayName();
        socialAuthBody.provider_id = result.getSignInAccount().getId();
        socialAuthBody.provider_name = result.getSignInAccount().getGivenName();

        myApiEndpointInterface.getUserDetails(socialAuthBody).enqueue(new Callback<SocialAuthResponse>() {
            @Override
            public void onResponse(Call<SocialAuthResponse> call, Response<SocialAuthResponse> response) {
                pb.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.isSuccessful() && response.body().status == 200) {
                        setToken(EmailLoginActivity.this, response.body().token);
                        Common.toast(EmailLoginActivity.this, response.body().message);
                    } else {
                        Common.toast(EmailLoginActivity.this, response.body().message);
                    }
                } else {
                    Common.toast(EmailLoginActivity.this, response.body().message);
                }
            }

            @Override
            public void onFailure(Call<SocialAuthResponse> call, Throwable t) {
                pb.setVisibility(View.GONE);
                Common.toast(EmailLoginActivity.this, t.getLocalizedMessage());
            }
        });
    }


    private void getUserProfile(AccessToken currentAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                currentAccessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.d("TAG", object.toString());
                        try {
                            String first_name = object.getString("first_name");
                            String last_name = object.getString("last_name");
                            String email = object.getString("email");
                            String id = object.getString("id");
                            String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();
    }

    public boolean validEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    private void onClickListener() {
        sign_in.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.text_signin) {
            _email = email.getText().toString();
            _password = password.getText().toString();

            if (!validEmail(_email)) {
                email.setError("Email is not Valid");
            } else if (!isValid(_password)) {
                password.setError("Password is not valid");
            } else {
                pb.setVisibility(View.VISIBLE);
                callSignInEmailAuthenticateApi(_email, _password, deviceName);
            }
        }
    }
}

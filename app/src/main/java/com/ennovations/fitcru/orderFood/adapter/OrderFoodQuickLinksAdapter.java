package com.ennovations.fitcru.orderFood.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.OrderFoodQuickLinksPojo;
import com.ennovations.fitcru.pojo.OrderFoodTodaySpecialMenuPojo;
import java.util.List;

public class OrderFoodQuickLinksAdapter extends RecyclerView.Adapter<OrderFoodQuickLinksAdapter.MyViewHolder> {

    private final List<OrderFoodQuickLinksPojo> itemsList;

    public OrderFoodQuickLinksAdapter(List<OrderFoodQuickLinksPojo> mItemList) {
        this.itemsList = mItemList;
    }

    @Override
    public OrderFoodQuickLinksAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_food_quick_link, parent, false);
        return new OrderFoodQuickLinksAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OrderFoodQuickLinksAdapter.MyViewHolder holder, final int position) {
        final OrderFoodQuickLinksPojo item = itemsList.get(position);
        holder.quick_food_image.setImageResource(item.getImg());
        holder.quick_food_name.setText(item.getName());
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView quick_food_image;
        TextView quick_food_name;

        public MyViewHolder(View itemView) {
            super(itemView);
            quick_food_image = (ImageView) itemView.findViewById(R.id.img_quick_food);
            quick_food_name = (TextView) itemView.findViewById(R.id.txt_quick_link_food_name);
        }
    }
}


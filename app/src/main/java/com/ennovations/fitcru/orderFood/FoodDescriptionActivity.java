package com.ennovations.fitcru.orderFood;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.ennovations.fitcru.R;
import com.ennovations.fitcru.orderFood.adapter.FodDescriptionIngredientsAdapter;
import com.ennovations.fitcru.pojo.IngredientsPojo;

import java.util.ArrayList;

public class FoodDescriptionActivity extends AppCompatActivity {

    RecyclerView rv_food_description_ingredients;
    ArrayList<IngredientsPojo> ingredientsList = new ArrayList();
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_description);

        rv_food_description_ingredients = findViewById(R.id.rv_food_des_ingredients);
        back = findViewById(R.id.img_back_order_food);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        prepareListForIngredients();
    }

    private void prepareListForIngredients() {

        ingredientsList.add(new IngredientsPojo("10", "Fat"));
        ingredientsList.add(new IngredientsPojo("107", "Carbs"));
        ingredientsList.add(new IngredientsPojo("8", "Fiber"));
        ingredientsList.add(new IngredientsPojo("28", "Protein"));
        ingredientsList.add(new IngredientsPojo("16", "Sugar"));
        ingredientsList.add(new IngredientsPojo("235", "Energy"));

        FodDescriptionIngredientsAdapter fodDescriptionIngredientsAdapter = new FodDescriptionIngredientsAdapter(ingredientsList);
        rv_food_description_ingredients.setAdapter(fodDescriptionIngredientsAdapter);
    }
}
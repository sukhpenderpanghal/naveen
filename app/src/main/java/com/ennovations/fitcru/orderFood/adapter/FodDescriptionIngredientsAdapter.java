package com.ennovations.fitcru.orderFood.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.IngredientsPojo;

import java.util.List;

public class FodDescriptionIngredientsAdapter extends RecyclerView.Adapter<FodDescriptionIngredientsAdapter.MyViewHolder> {

    private final List<IngredientsPojo> itemsList;

    public FodDescriptionIngredientsAdapter(List<IngredientsPojo> mItemList) {
        this.itemsList = mItemList;
    }

    @Override
    public FodDescriptionIngredientsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_food_ingredients, parent, false);
        return new FodDescriptionIngredientsAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FodDescriptionIngredientsAdapter.MyViewHolder holder, final int position) {
        final IngredientsPojo item = itemsList.get(position);
        holder.ingredient_value.setText(item.getKcal());
        holder.ingredient_name.setText(item.getName());
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView ingredient_value, ingredient_name;

        public MyViewHolder(View itemView) {
            super(itemView);
            ingredient_value = itemView.findViewById(R.id.txt_ingredients_value);
            ingredient_name = itemView.findViewById(R.id.txt_ingredients_name);
        }
    }
}



package com.ennovations.fitcru.orderFood.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ennovations.fitcru.R;
import com.ennovations.fitcru.orderFood.adapter.OrderFoodExploreMenuIndianAdapter;
import com.ennovations.fitcru.pojo.OrderFoodTodaySpecialMenuPojo;

import java.util.ArrayList;

public class ExploreMenuIndianFragment extends Fragment {

    RecyclerView rv_order_food_explore_indian;
    ArrayList<OrderFoodTodaySpecialMenuPojo> exploreMenuList = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_explore_menu_indian, container, false);

        rv_order_food_explore_indian = (RecyclerView) view.findViewById(R.id.rv_order_food_explore_menu_indian);
        rv_order_food_explore_indian.setHasFixedSize(true);
        rv_order_food_explore_indian.setLayoutManager(new LinearLayoutManager(getContext()));

        prepareListExploreMenuIndian();

        return view;

    }

    private void prepareListExploreMenuIndian() {
        exploreMenuList.add(new OrderFoodTodaySpecialMenuPojo("$480.00, ", "Butter Chicken (4 Pcs)", "293 kcal", R.drawable.butter_chicken));
        exploreMenuList.add(new OrderFoodTodaySpecialMenuPojo("$180.00, ", "Chicken Malai Tikka (8 Pcs)", "353 kcal", R.drawable.chicken_malyai_tikka));
        exploreMenuList.add(new OrderFoodTodaySpecialMenuPojo("$480.00, ", "Butter Chicken (4 Pcs)", "293 kcal", R.drawable.butter_chicken));
        exploreMenuList.add(new OrderFoodTodaySpecialMenuPojo("$180.00, ", "Chicken Malai Tikka (8 Pcs)", "353 kcal", R.drawable.chicken_malyai_tikka));
        exploreMenuList.add(new OrderFoodTodaySpecialMenuPojo("$480.00, ", "Butter Chicken (4 Pcs)", "293 kcal", R.drawable.butter_chicken));
        exploreMenuList.add(new OrderFoodTodaySpecialMenuPojo("$180.00, ", "Chicken Malai Tikka (8 Pcs)", "353 kcal", R.drawable.chicken_malyai_tikka));
        exploreMenuList.add(new OrderFoodTodaySpecialMenuPojo("$480.00, ", "Butter Chicken (4 Pcs)", "293 kcal", R.drawable.butter_chicken));
        exploreMenuList.add(new OrderFoodTodaySpecialMenuPojo("$180.00, ", "Chicken Malai Tikka (8 Pcs)", "353 kcal", R.drawable.chicken_malyai_tikka));

        OrderFoodExploreMenuIndianAdapter orderFoodTodaySpecialMenuAdapter = new OrderFoodExploreMenuIndianAdapter(exploreMenuList,getContext());
        rv_order_food_explore_indian.setAdapter(orderFoodTodaySpecialMenuAdapter);
    }
}
package com.ennovations.fitcru.orderFood.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.ennovations.fitcru.R;
import com.ennovations.fitcru.orderFood.FoodDescriptionActivity;
import com.ennovations.fitcru.pojo.OrderFoodTodaySpecialMenuPojo;

import java.util.List;

public class OrderFoodTodaySpecialMenuAdapter extends RecyclerView.Adapter<OrderFoodTodaySpecialMenuAdapter.MyViewHolder> {

    private final List<OrderFoodTodaySpecialMenuPojo> itemsList;
    private Context context;

    public OrderFoodTodaySpecialMenuAdapter(List<OrderFoodTodaySpecialMenuPojo> mItemList, Context context) {
        this.itemsList = mItemList;
        this.context = context;
    }

    @Override
    public OrderFoodTodaySpecialMenuAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_food_special_menu, parent, false);
        return new OrderFoodTodaySpecialMenuAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OrderFoodTodaySpecialMenuAdapter.MyViewHolder holder, final int position) {
        final OrderFoodTodaySpecialMenuPojo item = itemsList.get(position);
        holder.special_food_image.setImageResource(item.getImage());
        holder.food_name.setText(item.getName());
        holder.food_price.setText(item.getPrice());
        holder.food_energy.setText(item.getEnergyCalory());
        holder.special_food_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FoodDescriptionActivity.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView special_food_image;
        TextView food_name, food_price, food_energy;

        public MyViewHolder(View itemView) {
            super(itemView);
            special_food_image = (ImageView) itemView.findViewById(R.id.img_special_food);
            food_energy = (TextView) itemView.findViewById(R.id.txt_special_menu_food_energy);
            food_name = (TextView) itemView.findViewById(R.id.txt_special_menu_food_name);
            food_price = (TextView) itemView.findViewById(R.id.txt_special_menu_food_price);
        }
    }
}


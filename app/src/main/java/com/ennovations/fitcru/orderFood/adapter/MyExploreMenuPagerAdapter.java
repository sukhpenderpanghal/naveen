package com.ennovations.fitcru.orderFood.adapter;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ennovations.fitcru.orderFood.fragments.ExploreContinentalFragment;
import com.ennovations.fitcru.orderFood.fragments.ExploreDessertsFragment;
import com.ennovations.fitcru.orderFood.fragments.ExploreDrinksFragment;
import com.ennovations.fitcru.orderFood.fragments.ExploreMenuIndianFragment;
import com.ennovations.fitcru.orderFood.fragments.ExploreSidesFragment;

public class MyExploreMenuPagerAdapter extends FragmentPagerAdapter {

    private final Context myContext;
    int totalTabs;

    public MyExploreMenuPagerAdapter(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                ExploreMenuIndianFragment exploreMenuIndianFragment = new ExploreMenuIndianFragment();
                return exploreMenuIndianFragment;
            case 1:
                ExploreContinentalFragment exploreContinentalFragment = new ExploreContinentalFragment();
                return exploreContinentalFragment;
            case 2:
                ExploreSidesFragment exploreSidesFragment = new ExploreSidesFragment();
                return exploreSidesFragment;
            case 3:
                ExploreDrinksFragment exploreDrinksFragment = new ExploreDrinksFragment();
                return exploreDrinksFragment;
            case 4:
                ExploreDessertsFragment exploreDessertsFragment = new ExploreDessertsFragment();
                return exploreDessertsFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return totalTabs;
    }
}

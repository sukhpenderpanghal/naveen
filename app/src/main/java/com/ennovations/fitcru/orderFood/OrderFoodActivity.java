package com.ennovations.fitcru.orderFood;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.ennovations.fitcru.R;
import com.ennovations.fitcru.orderFood.adapter.MyExploreMenuPagerAdapter;
import com.ennovations.fitcru.orderFood.adapter.OrderFoodExploreMenuIndianAdapter;
import com.ennovations.fitcru.orderFood.adapter.OrderFoodQuickLinksAdapter;
import com.ennovations.fitcru.orderFood.adapter.OrderFoodTodaySpecialMenuAdapter;
import com.ennovations.fitcru.pojo.OrderFoodQuickLinksPojo;
import com.ennovations.fitcru.pojo.OrderFoodTodaySpecialMenuPojo;
import com.ennovations.fitcru.workout.adapter.MyAdapter;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class OrderFoodActivity extends AppCompatActivity {

    RecyclerView rv_order_today, rv_order_food_quick_link,rv_order_food_explore_indian;
    ArrayList<OrderFoodTodaySpecialMenuPojo> todaySpecialItemList = new ArrayList<>();
    ArrayList<OrderFoodQuickLinksPojo> todayQuickLinkItemList = new ArrayList<>();
    ViewPager viewPager;
    TabLayout tabLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_food);

        tabLayout = (TabLayout) findViewById(R.id.order_food_explore_menu_tab_layout);
        viewPager = (ViewPager) findViewById(R.id.viewPager_order_food);
        rv_order_today = (RecyclerView) findViewById(R.id.rv_order_food_today_items);
        rv_order_food_quick_link = (RecyclerView) findViewById(R.id.rv_order_food_quick_links);

        tabLayout.addTab(tabLayout.newTab().setText("Indian"));
        tabLayout.addTab(tabLayout.newTab().setText("Continental"));
        tabLayout.addTab(tabLayout.newTab().setText("Sides"));
        tabLayout.addTab(tabLayout.newTab().setText("Drinks"));
        tabLayout.addTab(tabLayout.newTab().setText("Desserts"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);

        findIds();
        addTabs();

        final MyExploreMenuPagerAdapter adapter = new MyExploreMenuPagerAdapter(this, getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        prepareList();
        PrepareQuickLinkList();
    }

    private void findIds() {

    }

    private void addTabs() {

    }

    private void PrepareQuickLinkList() {

        todayQuickLinkItemList.add(new OrderFoodQuickLinksPojo(R.drawable.pizza, "Pizza"));
        todayQuickLinkItemList.add(new OrderFoodQuickLinksPojo(R.drawable.burger, "Burger"));
        todayQuickLinkItemList.add(new OrderFoodQuickLinksPojo(R.drawable.pizza, "Pizza"));
        todayQuickLinkItemList.add(new OrderFoodQuickLinksPojo(R.drawable.burger, "Burger"));
        todayQuickLinkItemList.add(new OrderFoodQuickLinksPojo(R.drawable.pizza, "Pizza"));
        todayQuickLinkItemList.add(new OrderFoodQuickLinksPojo(R.drawable.burger, "Burger"));
        todayQuickLinkItemList.add(new OrderFoodQuickLinksPojo(R.drawable.pizza, "Pizza"));
        todayQuickLinkItemList.add(new OrderFoodQuickLinksPojo(R.drawable.burger, "Burger"));

        OrderFoodQuickLinksAdapter orderFoodQuickLinksAdapter = new OrderFoodQuickLinksAdapter(todayQuickLinkItemList);
        rv_order_food_quick_link.setAdapter(orderFoodQuickLinksAdapter);
    }

    private void prepareList() {

        todaySpecialItemList.add(new OrderFoodTodaySpecialMenuPojo("$480.00", "Butter Chicken", "293 kcal", R.drawable.butter_chicken));
        todaySpecialItemList.add(new OrderFoodTodaySpecialMenuPojo("$180.00", "Chicken Malai Tikka", "353 kcal", R.drawable.chicken_malyai_tikka));
        todaySpecialItemList.add(new OrderFoodTodaySpecialMenuPojo("$480.00", "Butter Chicken", "293 kcal", R.drawable.butter_chicken));
        todaySpecialItemList.add(new OrderFoodTodaySpecialMenuPojo("$180.00", "Chicken Malai Tikka", "353 kcal", R.drawable.chicken_malyai_tikka));
        todaySpecialItemList.add(new OrderFoodTodaySpecialMenuPojo("$480.00", "Butter Chicken", "293 kcal", R.drawable.butter_chicken));
        todaySpecialItemList.add(new OrderFoodTodaySpecialMenuPojo("$180.00", "Chicken Malai Tikka", "353 kcal", R.drawable.chicken_malyai_tikka));
        todaySpecialItemList.add(new OrderFoodTodaySpecialMenuPojo("$480.00", "Butter Chicken", "293 kcal", R.drawable.butter_chicken));
        todaySpecialItemList.add(new OrderFoodTodaySpecialMenuPojo("$180.00", "Chicken Malai Tikka", "353 kcal", R.drawable.chicken_malyai_tikka));


        OrderFoodTodaySpecialMenuAdapter orderFoodTodaySpecialMenuAdapter = new OrderFoodTodaySpecialMenuAdapter(todaySpecialItemList,OrderFoodActivity.this);
        rv_order_today.setAdapter(orderFoodTodaySpecialMenuAdapter);
    }
}
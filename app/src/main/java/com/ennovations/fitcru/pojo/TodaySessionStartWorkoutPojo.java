package com.ennovations.fitcru.pojo;

public class TodaySessionStartWorkoutPojo {

    public String w_time;
    public String e_name;

    public TodaySessionStartWorkoutPojo(String workout_time, String ex_name) {
        this.w_time = workout_time;
        this.e_name = ex_name;
    }


    public String getTime() {
        return w_time;
    }

    public String getWorkOutName() {
        return e_name;
    }
}

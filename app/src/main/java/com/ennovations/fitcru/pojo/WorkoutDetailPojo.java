package com.ennovations.fitcru.pojo;

public class WorkoutDetailPojo {

    public String id;
    public String time;
    public String workout_name;
    public String reps;
    public String weight;

    public WorkoutDetailPojo(String id, String time, String workout_name, String reps, String weight)
    {
        this.id = id;
        this.time = time;
        this.workout_name = workout_name;
        this.reps = reps;
        this.weight = weight;
    }

    public String getId() {
        return id;
    }

    public String getWorkout_name() {
        return workout_name;
    }

    public String getTime() {
        return time;
    }

    public String getReps() {
        return reps;
    }

    public String getWeight() {
        return weight;
    }
}

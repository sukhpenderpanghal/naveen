package com.ennovations.fitcru.pojo;

public class PlanDetailFeedbackPojo {

    public String id;
    public String image;
    public String description;

    public PlanDetailFeedbackPojo(String id, String image, String description)
    {
        this.id = id;
        this.image = image;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getImage() {
        return image;
    }

    public String getDescription() {
        return description;
    }
}

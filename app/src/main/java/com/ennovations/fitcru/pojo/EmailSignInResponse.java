package com.ennovations.fitcru.pojo;

import java.util.List;

public class EmailSignInResponse {

    public String message;
    public int status;
    public User user;
    public String token;
}


package com.ennovations.fitcru.pojo;

import java.util.Date;
import java.util.List;

public class UpDateClientDataPojo {
    public String name;
    public String email;
    public Date created_at;
    public Date updated_at;
    public int id;
    public int user_id;
    public String mobile;
    public String gender;
    public String dob;
    public String weight;
    public String height;
    public String how_active;
    public String fitness_level;
    public String food_preference;
    public Object what_brings;
    public int goal_weight;
    public Object reason_change;
    public Object upcoming_event;
    public int coach_id;
    public int age;
    public List<Object> plans;
    public List<Object> challenges;
    public double bmi;
    public String bmi_result;
    public String bmi_alert;
    public double bmr;
    public double tdee;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getHow_active() {
        return how_active;
    }

    public void setHow_active(String how_active) {
        this.how_active = how_active;
    }

    public String getFitness_level() {
        return fitness_level;
    }

    public void setFitness_level(String fitness_level) {
        this.fitness_level = fitness_level;
    }

    public String getFood_preference() {
        return food_preference;
    }

    public void setFood_preference(String food_preference) {
        this.food_preference = food_preference;
    }

    public Object getWhat_brings() {
        return what_brings;
    }

    public void setWhat_brings(Object what_brings) {
        this.what_brings = what_brings;
    }

    public int getGoal_weight() {
        return goal_weight;
    }

    public void setGoal_weight(int goal_weight) {
        this.goal_weight = goal_weight;
    }

    public Object getReason_change() {
        return reason_change;
    }

    public void setReason_change(Object reason_change) {
        this.reason_change = reason_change;
    }

    public Object getUpcoming_event() {
        return upcoming_event;
    }

    public void setUpcoming_event(Object upcoming_event) {
        this.upcoming_event = upcoming_event;
    }

    public int getCoach_id() {
        return coach_id;
    }

    public void setCoach_id(int coach_id) {
        this.coach_id = coach_id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<Object> getPlans() {
        return plans;
    }

    public void setPlans(List<Object> plans) {
        this.plans = plans;
    }

    public List<Object> getChallenges() {
        return challenges;
    }

    public void setChallenges(List<Object> challenges) {
        this.challenges = challenges;
    }

    public double getBmi() {
        return bmi;
    }

    public void setBmi(double bmi) {
        this.bmi = bmi;
    }

    public String getBmi_result() {
        return bmi_result;
    }

    public void setBmi_result(String bmi_result) {
        this.bmi_result = bmi_result;
    }

    public String getBmi_alert() {
        return bmi_alert;
    }

    public void setBmi_alert(String bmi_alert) {
        this.bmi_alert = bmi_alert;
    }

    public double getBmr() {
        return bmr;
    }

    public void setBmr(double bmr) {
        this.bmr = bmr;
    }

    public double getTdee() {
        return tdee;
    }

    public void setTdee(double tdee) {
        this.tdee = tdee;
    }
}
package com.ennovations.fitcru.pojo;

import java.util.ArrayList;

class Equipment{
    public int id;
    public String name;
}

public class TodaySessionResponse{
    public String session_name;
    public String avg_mins;
    public String intensity;
    public String level;
    public String benefits;
    public ArrayList<Equipment> equipments;
    public ArrayList<ExercisePlan> exercise_plan;
}
package com.ennovations.fitcru.pojo;

public class StudioPojo {

    public String id;
    public String name;
    public String description;
    public int image;

    public StudioPojo(String id, String name,  String des,int img)
    {
        this.id = id;
        this.name = name;
        this.description = des;
        this.image = img;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

//    public int getImage() {
//        return image;
//    }

        public int getImage() {
        return image;
    }
}

package com.ennovations.fitcru.pojo;

public class TodaySessionCoreExercisePojo {

    public int image;
    public String time;
    public String e_name;

    public TodaySessionCoreExercisePojo(int image, String time, String ex_name) {
        this.image = image;
        this.time = time;
        this.e_name = ex_name;
    }

    public int getImage() {
        return image;
    }

    public String getTime() {
        return time;
    }

    public String getWorkOutName() {
        return e_name;
    }
}

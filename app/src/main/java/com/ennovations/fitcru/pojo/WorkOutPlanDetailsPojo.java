package com.ennovations.fitcru.pojo;

import java.util.List;

public class WorkOutPlanDetailsPojo{
    public int id;
    public String banner;
    public String title;
    public String description;
    public String features;
    public String created_at;
    public String updated_at;
    public int parent;
    public String message;
    public int status;
    public List<PlanDetails> plans;
}
package com.ennovations.fitcru.pojo;

public class SocialAuthResponse{
    public String message;
    public int status;
    public SocialAuthResponseUser socialAuthResponseUser;
    public String token;
}

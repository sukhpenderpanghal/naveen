package com.ennovations.fitcru.pojo;

public class ActivityAndFitnessPojo {
    private int id;
    private final boolean isCheck;
    private String name;
    private String reps;
    private boolean isChecked = false;

    public ActivityAndFitnessPojo(int id, boolean isCheck, String option, String rep) {
        this.id = id;
        this.isCheck = isChecked;
        this.name = option;
        this.reps = rep;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReps() {
        return reps;
    }

    public void setReps(String reps) {
        this.reps = reps;
    }

    public String getOption() {
        return name;
    }

    public void setOption(String option) {
        this.name = option;
    }
}

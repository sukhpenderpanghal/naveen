package com.ennovations.fitcru.pojo;

public class Workout {
    public int id;
    public String banner;
    public String seesion_name;
    public String description;
    public String types;
    public String studio;
    public String benefits;
    public String workouts;
    public String equipment;
    public String duration;
    public String intensity;
    public String level;
    public String tags;
    public String created_at;
    public String updated_at;
}

package com.ennovations.fitcru.pojo;

public class UpdateClientResponse {
    public String message;
    public int status;
    public UpDateClientDataPojo data;
}

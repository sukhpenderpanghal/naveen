package com.ennovations.fitcru.pojo;

import java.util.List;

public class ChallengeData{
    public List<ChallengeCurrent> challengeCurrents;
    public List<ChallengeUpcoming> challengeUpcomings;
}

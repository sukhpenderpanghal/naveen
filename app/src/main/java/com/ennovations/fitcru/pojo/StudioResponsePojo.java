package com.ennovations.fitcru.pojo;

import java.util.ArrayList;

class StudioWorkoutPojo{
    public int id;
    public String banner;
    public String seesion_name;
    public String description;
    public String types;
    public String studio;
    public String benefits;
    public String workouts;
    public String equipment;
    public String duration;
    public String intensity;
    public String level;
    public String tags;
    public String created_at;
    public String updated_at;
}

public class StudioResponsePojo{
    public String message;
    public int status;
    public String main_banner;
    public ArrayList<StudioData> data;
}
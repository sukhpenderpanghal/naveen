package com.ennovations.fitcru.pojo;

import java.util.List;

public class User {
    public Object name;
    public String email;
    public String created_at;
    public String updated_at;
    public int id;
    public int user_id;
    public Object mobile;
    public Object gender;
    public Object dob;
    public Object weight;

    public Object getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public int getId() {
        return id;
    }

    public int getUser_id() {
        return user_id;
    }

    public Object getMobile() {
        return mobile;
    }

    public Object getGender() {
        return gender;
    }

    public Object getDob() {
        return dob;
    }

    public Object getWeight() {
        return weight;
    }

    public Object getHeight() {
        return height;
    }

    public Object getHow_active() {
        return how_active;
    }

    public Object getFitness_level() {
        return fitness_level;
    }

    public Object getFood_preference() {
        return food_preference;
    }

    public Object getWhat_brings() {
        return what_brings;
    }

    public Object getGoal_weight() {
        return goal_weight;
    }

    public Object getReason_change() {
        return reason_change;
    }

    public String getUpcoming_event() {
        return upcoming_event;
    }

    public int getCoach_id() {
        return coach_id;
    }

    public int getAge() {
        return age;
    }

    public List<Object> getPlans() {
        return plans;
    }

    public List<Object> getChallenges() {
        return challenges;
    }

    public Object getBmi() {
        return bmi;
    }

    public Object getBmi_result() {
        return bmi_result;
    }

    public Object getBmi_alert() {
        return bmi_alert;
    }

    public Object getBmr() {
        return bmr;
    }

    public Object getTdee() {
        return tdee;
    }

    public Object height;
    public Object how_active;
    public Object fitness_level;
    public Object food_preference;
    public Object what_brings;
    public Object goal_weight;
    public Object reason_change;
    public String upcoming_event;
    public int coach_id;
    public int age;
    public List<Object> plans;
    public List<Object> challenges;
    public Object bmi;
    public Object bmi_result;
    public Object bmi_alert;
    public Object bmr;
    public Object tdee;
}

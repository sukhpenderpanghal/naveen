package com.ennovations.fitcru.pojo;

import java.util.List;

public class ChallengeCurrent {
    public int id;
    public String title;
    public String description;
    public String banner;
    public String price;
    public String duration;
    public List<Object> equipments;
    public String community_id;
    public String start_date;
    public String end_date;
    public int status;
    public String created_at;
    public String updated_at;
    public int total_members;
}

package com.ennovations.fitcru.pojo;

public class ExploreExercisePojo {

    public int image;
    public String time;
    public String workout_name;
    public String equips;
    public boolean status;

    public ExploreExercisePojo(int image, String time, String workout_name, String equip, Boolean stats) {
        this.image = image;
        this.time = time;
        this.workout_name = workout_name;
        this.equips = equip;
        this.status = stats;
    }

    public int getImage() {
        return image;
    }

    public String getTime() {
        return time;
    }

    public String getWorkout_name() {
        return workout_name;
    }

    public String getEquips() {
        return equips;
    }

    public boolean isStatus() {
        return status;
    }

}

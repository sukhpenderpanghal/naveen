package com.ennovations.fitcru.pojo;

public class OrderFoodTodaySpecialMenuPojo {

    public int image;
    public String name;
    public String price;
    public String energyCalory;


    public int getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getEnergyCalory() {
        return energyCalory;
    }

    public OrderFoodTodaySpecialMenuPojo(String price, String name, String energy, int image) {
        this.price = price;
        this.name = name;
        this.energyCalory = energy;
        this.image = image;
    }
}

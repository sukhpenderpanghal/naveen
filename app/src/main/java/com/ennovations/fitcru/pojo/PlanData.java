package com.ennovations.fitcru.pojo;

import java.util.List;

public class PlanData {
    public int id;
    public String banner;
    public String title;
    public String description;
    public String features;
    public String created_at;
    public String updated_at;
    public int parent;
    public List<PlanDetails> plans;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFeatures() {
        return features;
    }

    public void setFeatures(String features) {
        this.features = features;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public List<PlanDetails> getPlans() {
        return plans;
    }

    public void setPlans(List<PlanDetails> plans) {
        this.plans = plans;
    }
}


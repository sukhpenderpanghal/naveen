package com.ennovations.fitcru.pojo;

public class PlanPojo {

    public String id;
    public String name;
    public String description;
    public int image;

    public PlanPojo(String id, String name, String description, int image)
    {
        this.id = id;
        this.name = name;
        this.description = description;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public int getImage() {
        return image;
    }

    //    public String getImage() {
//        return image;
//    }
}

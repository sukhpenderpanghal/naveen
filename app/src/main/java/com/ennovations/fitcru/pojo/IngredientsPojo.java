package com.ennovations.fitcru.pojo;

public class IngredientsPojo {

    public String kcal;
    public String name;

    public String getKcal() {
        return kcal;
    }

    public String getName() {
        return name;
    }

    public IngredientsPojo(String kcal, String name){
        this.kcal = kcal;
        this.name = name;
    }
}

package com.ennovations.fitcru.pojo;

public class RegisterActive {


    public String id;
    public String text;

    public RegisterActive(String id, String text)
    {
        this.id = id;
        this.text = text;
    }


    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }
}


package com.ennovations.fitcru.pojo;

public class WorkOutChallengeResponse{
    public String message;
    public int status;
    public ChallengeData challengeData;
}

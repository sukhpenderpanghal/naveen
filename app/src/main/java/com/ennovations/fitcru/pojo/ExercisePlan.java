package com.ennovations.fitcru.pojo;

public class ExercisePlan {
    public int exercise_id;
    public int exercizetype_id;
    public String banner;
    public String video;

    public int getExercise_id() {
        return exercise_id;
    }

    public int getExercizetype_id() {
        return exercizetype_id;
    }

    public String getBanner() {
        return banner;
    }

    public String getVideo() {
        return video;
    }

    public String getTitle() {
        return title;
    }

    public int getSets() {
        return sets;
    }

    public String getTarget() {
        return target;
    }

    public String getRest() {
        return rest;
    }

    public String title;
    public int sets;
    public String target;
    public String rest;
}

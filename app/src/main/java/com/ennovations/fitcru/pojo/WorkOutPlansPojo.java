package com.ennovations.fitcru.pojo;

import java.util.List;

public class WorkOutPlansPojo {
    public String message;
    public int status;
    public List<PlanData> data;
}


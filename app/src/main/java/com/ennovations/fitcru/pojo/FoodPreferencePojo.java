package com.ennovations.fitcru.pojo;

public class FoodPreferencePojo {
    private int id;
    private boolean isCheck;
    private String option;

    public FoodPreferencePojo(int id,boolean isCheck, String option) {
        this.id=id;
        this.isCheck = isChecked;
        this.option=option;
    }
    private boolean isChecked = false;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }
}

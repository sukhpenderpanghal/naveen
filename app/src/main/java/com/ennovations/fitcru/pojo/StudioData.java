package com.ennovations.fitcru.pojo;

import java.util.ArrayList;

public class StudioData {
    public int id;
    public String created_at;
    public String updated_at;
    public String banner;

    public int getId() {
        return id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getBanner() {
        return banner;
    }

    public String getTitle() {
        return title;
    }

    public int getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }

    public ArrayList<StudioWorkoutPojo> getWorkout() {
        return workout;
    }

    public String title;
    public int status;
    public String description;
    public ArrayList<StudioWorkoutPojo> workout;
}

package com.ennovations.fitcru.pojo;

public class SessionPojo {

    public String id;
    public String name;
    public String description;
    public int image;
    public String data;

    public SessionPojo(String id, String name,  int image,  String description,String data)
    {
        this.id = id;
        this.name = name;
        this.image = image;
        this.data = data;
        this.description = description;
    }



    public String getData() {
        return data;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public int getImage() {
        return image;
    }

//    public String getImage() {
//        return image;
//    }
}

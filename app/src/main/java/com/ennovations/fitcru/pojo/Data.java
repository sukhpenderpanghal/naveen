package com.ennovations.fitcru.pojo;

import java.util.ArrayList;
import java.util.List;

public class Data {
    public int id;
    public String created_at;
    public String updated_at;
    public String banner;
    public String title;
    public int status;
    public String description;
    public List<Workout> workout;
}

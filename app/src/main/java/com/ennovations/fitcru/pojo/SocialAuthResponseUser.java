package com.ennovations.fitcru.pojo;

import java.util.Date;

public class SocialAuthResponseUser {
    public String name;
    public Object email;
    public Date created_at;
    public Date updated_at;
    public int id;
    public int user_id;
    public String mobile;
    public Object gender;
    public Object dob;
    public Object weight;
    public Object height;
    public Object how_active;
    public Object fitness_level;
    public Object food_preference;
    public Object what_brings;
    public Object goal_weight;
    public Object reason_change;
    public Object upcoming_event;
    public int coach_id;
    public Object short_term_goal_week;
    public Object short_term_goal_weight;
    public Object long_term_goal_weight;
    public Object long_term_goal_week;
    public Object plan_id;
    public Object start_date;
    public Object end_date;
    public Object title;
    public Object bmi;
    public Object bmi_result;
    public Object bmi_alert;
    public Object bmr;
    public Object tdee;
}

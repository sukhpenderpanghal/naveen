package com.ennovations.fitcru.pojo;

import java.util.List;

class Plan{
    public int plan_id;
    public String title;
    public String start_date;
    public String end_date;
    public String banner;
    public int duration;
    public String time_interval;
    public String cost;
}

class Challenge{
    public int challenge_id;
    public String title;
    public String banner;
    public String price;
    public String duration;
    public String start_date;
    public String end_date;
}

class UserData{
    public String name;
    public String email;
    public String created_at;
    public String updated_at;
    public int id;
    public int user_id;
    public String mobile;
    public String gender;
    public String dob;
    public String weight;
    public String height;
    public String how_active;
    public String fitness_level;
    public String food_preference;
    public String what_brings;
    public int goal_weight;
    public String reason_change;
    public String upcoming_event;
    public int coach_id;
    public int age;
    public List<Plan> plans;
    public List<Challenge> challenges;
    public double bmi;
    public String bmi_result;
    public String bmi_alert;
    public double bmr;
    public double tdee;
}

public class EmailAuthenticationResponse{
    public String message;
    public int status;
    public UserData user;
    public String token;
}


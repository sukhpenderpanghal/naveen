package com.ennovations.fitcru.pojo;

public class CommonResponse {
    private String message;
    private long status;

    public CommonResponse(String message, long status) {
        this.message = message;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }
}

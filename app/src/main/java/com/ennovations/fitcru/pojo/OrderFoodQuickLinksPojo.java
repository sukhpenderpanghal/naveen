package com.ennovations.fitcru.pojo;

public class OrderFoodQuickLinksPojo {

    public int img;
    public String name;

    public int getImg() {
        return img;
    }

    public String getName() {
        return name;
    }

    public OrderFoodQuickLinksPojo(int image, String name) {
        this.img = image;
        this.name = name;
    }
}

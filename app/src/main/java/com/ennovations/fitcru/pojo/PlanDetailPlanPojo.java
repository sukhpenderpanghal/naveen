package com.ennovations.fitcru.pojo;

public class PlanDetailPlanPojo {

    public String id;
    public String plan_price;
    public String plan_name;
    public String plan_time;

    public PlanDetailPlanPojo(String id, String plan_price, String plan_name, String plan_time)
    {
        this.id = id;
        this.plan_price = plan_price;
        this.plan_name = plan_name;
        this.plan_time = plan_time;
    }

    public String getId() {
        return id;
    }

    public String getPlan_name() {
        return plan_name;
    }

    public String getPlan_price() {
        return plan_price;
    }

    public String getPlan_time() {
        return plan_time;
    }
}

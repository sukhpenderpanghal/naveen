package com.ennovations.fitcru.pojo;

public class DataClass {
    private int id;
    private String option;

    public DataClass(int id, String option) {
    this.id=id;
    this.option=option;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }
}

package com.ennovations.fitcru.pojo;

public class ChallengePojo {

    public int image;
    public String time_left;
    public String title;
    public String description;
    public String equipment;
    public String price;
    public String slot_remaining;


    public ChallengePojo(int image, String titlee, String des, String time, String equip, String pric, String slots_left) {
        this.image = image;
        this.title = titlee;
        this.description = des;
        this.time_left = time;
        this.equipment = equip;
        this.price = pric;
        this.slot_remaining = slots_left;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTime_left() {
        return time_left;
    }

    public void setTime_left(String time_left) {
        this.time_left = time_left;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSlot_remaining() {
        return slot_remaining;
    }

    public void setSlot_remaining(String slot_remaining) {
        this.slot_remaining = slot_remaining;
    }
}

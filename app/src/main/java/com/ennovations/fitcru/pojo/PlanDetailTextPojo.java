package com.ennovations.fitcru.pojo;

public class PlanDetailTextPojo {
    public String id;
    public String text;

    public PlanDetailTextPojo(String id, String text)
    {
        this.id = id;
        this.text = text;
    }


    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }

}

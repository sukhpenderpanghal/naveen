package com.ennovations.fitcru;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ennovations.fitcru.adapter.NutritionTrackAdapter;
import com.ennovations.fitcru.pojo.NutritionTrack;

import org.joda.time.DateTime;
import java.util.Calendar;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;


public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    NutritionTrackAdapter nutritionTrackAdapter;
    NutritionTrack[] nutritionTracks;
    TextView text_kcal, text_protien, text_fat, text_curb, textView;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        text_kcal = (TextView) findViewById(R.id.text_kcal);
        text_fat = (TextView) findViewById(R.id.text_fat);
        text_curb = (TextView) findViewById(R.id.text_carbs);
        text_protien = (TextView) findViewById(R.id.text_protiens);
        back = (ImageView) findViewById(R.id.image_backpressed);
        textView = (TextView) findViewById(R.id.text);

        textView.setText(R.string.lbl_nutrition_tracking);

        nutritionTracks = new NutritionTrack[]{
                new NutritionTrack("8.00 AM" ,"44", "1100", "BreakFast"),
                new NutritionTrack("12.00 PM" ,"50", "1600", "Meal"),
                new NutritionTrack("4.00 PM" ,"44", "1100", "BreakFast"),
                new NutritionTrack("8.00 PM" ,"60", "2100", "Dinner"),
        };

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));

        nutritionTrackAdapter = new NutritionTrackAdapter(nutritionTracks);
        recyclerView.setAdapter(nutritionTrackAdapter);

        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);

        /* ends after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        final Calendar defaultSelectedDate = Calendar.getInstance();


        HorizontalCalendar horizontalCalendar = new HorizontalCalendar.Builder(this, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(5)
                .configure()
                .formatMiddleText("EEE")
                .formatBottomText("dd")
                .showBottomText(true)
                .showTopText(false)
                .end()
                .defaultSelectedDate(defaultSelectedDate)
                .build();


        Log.i("Default Date", DateFormat.format("EEE, MMM d, yyyy", defaultSelectedDate).toString());
        String a = String.valueOf(DateFormat.format("MMM d", horizontalCalendar.getDateAt(0)).toString());
        String b = String.valueOf(horizontalCalendar.getDateAt(4));
        Log.i("Default", a +"- "+horizontalCalendar.getDateAt(4) + " - Position = ");


        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                String selectedDateStr = DateFormat.format("EEE, MMM d, yyyy", date).toString();
                Toast.makeText(MainActivity.this, selectedDateStr + " selected!", Toast.LENGTH_SHORT).show();
                String a = String.valueOf(horizontalCalendar.getDateAt(0));
                String b = String.valueOf(horizontalCalendar.getDateAt(4));
                Log.i("onDateSelected", a +"- "+b + " - Position = " + position);
                Log.i("onDateSelected", selectedDateStr + " - Position = " + position);
            }

        });



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }


}
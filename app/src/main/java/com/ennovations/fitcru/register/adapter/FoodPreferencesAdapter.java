package com.ennovations.fitcru.register.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.ChallengePojo;
import com.ennovations.fitcru.pojo.DataClass;
import com.ennovations.fitcru.pojo.FoodPreferencePojo;

import java.util.List;

public class FoodPreferencesAdapter extends RecyclerView.Adapter<FoodPreferencesAdapter.MyViewHolder> {

    private final List<FoodPreferencePojo> itemsList;
    Context context;

    public FoodPreferencesAdapter(List<FoodPreferencePojo> mItemList, Context context) {
        this.itemsList = mItemList;
        this.context = context;
    }

    @Override
    public FoodPreferencesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_food_preferences, parent, false);
        return new FoodPreferencesAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FoodPreferencesAdapter.MyViewHolder holder, final int position) {
        final FoodPreferencePojo item = itemsList.get(position);
        holder.text.setText(item.getOption());
        holder.imageView.setVisibility(item.isChecked() ? View.VISIBLE : View.GONE);

        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                item.setChecked(!item.isChecked());
                if (item.isChecked()){
                    holder.imageView.setVisibility(View.VISIBLE);
                    holder.ll.setBackgroundResource(R.drawable.custom_rounded_filled_black_color);
                    holder.text.setTextColor(context.getResources().getColor(R.color.white));

                }else {
                    holder.imageView.setVisibility(View.GONE);
                    holder.ll.setBackgroundResource(R.drawable.custom_rounded_filled_white_with_border_black_color);
                    holder.text.setTextColor(context.getResources().getColor(R.color.black));


                }
                //holder.imageView.setVisibility(item.isChecked() ? View.VISIBLE : View.GONE);
            }
        });

    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView text;
        ImageView imageView;
        LinearLayout ll;

        public MyViewHolder(View itemView) {
            super(itemView);
            text = (TextView) itemView.findViewById(R.id.textView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            ll = (LinearLayout) itemView.findViewById(R.id.lin_lay_f_preference);
        }
    }
}



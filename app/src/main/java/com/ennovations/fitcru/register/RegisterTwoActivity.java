package com.ennovations.fitcru.register;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.ennovations.fitcru.R;

public class RegisterTwoActivity extends AppCompatActivity {

    SharedPreferences sp;
    SharedPreferences.Editor editor;
    EditText height;
    String _height;
    TextView save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_two);

        sp = getSharedPreferences(RegisterActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sp.edit();

        height = (EditText) findViewById(R.id.texT_height);
        save = (TextView) findViewById(R.id.txt_save);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _height = height.getText().toString();

                editor.putString("height",_height);
                editor.commit();
                Intent intent = new Intent(RegisterTwoActivity.this,RegisterThreeActivity.class);
                startActivity(intent);
            }
        });


    }
}
package com.ennovations.fitcru.register;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ennovations.fitcru.R;
import com.ennovations.fitcru.adapter.RecyclerviewItemAdapter;
import com.ennovations.fitcru.pojo.ActivityAndFitnessPojo;
import com.ennovations.fitcru.pojo.RegisterActive;
import com.ennovations.fitcru.register.adapter.ActiveYouAreAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RegisterFiveActivity extends AppCompatActivity {

    TextView save;
    RecyclerView recyclerView, rv_fitness;
    RegisterActive registerActive;
    ImageView back;
    ArrayList<RegisterActive> registerActives = new ArrayList<>();
    RadioGroup radioGroup;
    RadioButton radioButton;
    ArrayList<ActivityAndFitnessPojo> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_five);

        save = findViewById(R.id.txt_save);
        back = findViewById(R.id.img_back_4);
        radioGroup = findViewById(R.id.radio_group);
        recyclerView = findViewById(R.id.rv_active_lvl);
        rv_fitness = findViewById(R.id.rv_fitness_lvl);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        prepareList();
        //getdata();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterFiveActivity.this, RegisterSixActivity.class);
                startActivity(intent);
                finish();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterFiveActivity.this, RegisterSevenActivity.class);
                startActivity(intent);
                finish();
                /*int selectedId = radioGroup.getCheckedRadioButtonId();
                radioButton = (RadioButton) findViewById(selectedId);

                if (selectedId == -1)
                {
                    Toast.makeText(RegisterFiveActivity.this,"Nothing selected, Please select", Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent intent = new Intent(RegisterFiveActivity.this, RegisterSixActivity.class);
                    startActivity(intent);
                }*/
            }
        });


    }

    private void prepareList() {
        list.add(new ActivityAndFitnessPojo(1, false, "Very light", "0-5k steps"));
        list.add(new ActivityAndFitnessPojo(2, false, "Lightly Active", "7k steps"));
        list.add(new ActivityAndFitnessPojo(3, false, "Active", "10k steps"));
        list.add(new ActivityAndFitnessPojo(4, false, "Intense", "15k steps"));
        list.add(new ActivityAndFitnessPojo(5, false, "Very Intense", "20k steps"));

        ActiveYouAreAdapter activeYouAreAdapter = new ActiveYouAreAdapter(list, RegisterFiveActivity.this);
        recyclerView.setAdapter(activeYouAreAdapter);
        rv_fitness.setAdapter(activeYouAreAdapter);

    }

    public void getdata() {
        String url_l = "http://65.1.160.150/api/master/fitnesslevel";
        Log.i("url", url_l);
        StringRequest requestt = new StringRequest(url_l, new Response.Listener<String>() {
            @Override
            public void onResponse(String string) {
                try {

                    JSONObject jsonObject = new JSONObject(string);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");


                    for (int i = 0; i < jsonArray.length(); ++i) {

                        JSONObject c = jsonArray.getJSONObject(i);

                        String a = c.getString("id");
                        String b = c.getString("name");
                        String d = c.getString("detail");
                        String e = c.getString("level");

                        RegisterActive registerActive = new RegisterActive(a, b);
                        registerActives.add(registerActive);


                    }

                    RecyclerviewItemAdapter recyclerviewItemAdapter = new RecyclerviewItemAdapter(registerActives);
                    recyclerView.setAdapter(recyclerviewItemAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getApplicationContext(), "Check Your Network Connection", Toast.LENGTH_SHORT).show();

            }
        });

        RequestQueue rQueue = Volley.newRequestQueue(RegisterFiveActivity.this);
        rQueue.add(requestt);
    }
}
 
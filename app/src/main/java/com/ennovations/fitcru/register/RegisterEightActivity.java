package com.ennovations.fitcru.register;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.ennovations.fitcru.R;

public class RegisterEightActivity extends AppCompatActivity {

    TextView save;
    EditText goal_weight;
    String _weight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_eight);

        save = (TextView) findViewById(R.id.txt_save);
        goal_weight = (EditText) findViewById(R.id.texT_goal_weight);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _weight = goal_weight.getText().toString();

                Intent intent = new Intent(RegisterEightActivity.this,RegisterNineActivity.class);
                startActivity(intent);
            }
        });

    }
}
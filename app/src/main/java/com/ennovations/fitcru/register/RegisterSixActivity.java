package com.ennovations.fitcru.register;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ennovations.fitcru.R;
import com.ennovations.fitcru.adapter.RecyclerviewItemAdapter;
import com.ennovations.fitcru.pojo.FoodPreferencePojo;
import com.ennovations.fitcru.pojo.RegisterActive;
import com.ennovations.fitcru.register.adapter.FoodPreferencesAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RegisterSixActivity extends AppCompatActivity {

    TextView save;
    RecyclerView recyclerView;
    RegisterActive registerActive;
    ImageView back;
    ArrayList<RegisterActive> registerActives = new ArrayList<>();
    RadioGroup radioGroup;
    ArrayList<FoodPreferencePojo> list = new ArrayList<>();
    RadioButton radioButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_six);

        save = findViewById(R.id.txt_save);
        radioGroup = findViewById(R.id.radio_group);
        recyclerView = findViewById(R.id.recyclerview);
        back = findViewById(R.id.img_back_3);


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        //getdata();

        prepareList();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterSixActivity.this, RegistterOneActivity.class);
                startActivity(intent);
                finish();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(RegisterSixActivity.this, RegisterFiveActivity.class);
                startActivity(intent);
                finish();
                /*int selectedId = radioGroup.getCheckedRadioButtonId();
                radioButton = findViewById(selectedId);

                if (selectedId == -1) {
                    Toast.makeText(RegisterSixActivity.this, "Nothing selected, Please select", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(RegisterSixActivity.this, RegisterSevenActivity.class);
                    startActivity(intent);
                }*/

            }
        });


    }

    private void prepareList() {
        list.add(new FoodPreferencePojo(1, false, "Vegetarian"));
        list.add(new FoodPreferencePojo(2, false, "Non-Vegetarian"));
        list.add(new FoodPreferencePojo(3, false, "Vegan"));
        list.add(new FoodPreferencePojo(4, false, "Join"));
        list.add(new FoodPreferencePojo(5, false, "Eggetarian"));
        list.add(new FoodPreferencePojo(6, false, "Pescatarian"));
        list.add(new FoodPreferencePojo(7, false, "Halal Only"));
        list.add(new FoodPreferencePojo(8, false, "Friendly"));
        list.add(new FoodPreferencePojo(9, false, "Other"));
        list.add(new FoodPreferencePojo(10, false, "More"));

        FoodPreferencesAdapter foodPreferencesAdapter = new FoodPreferencesAdapter(list, RegisterSixActivity.this);
        recyclerView.setAdapter(foodPreferencesAdapter);

    }

    public void getdata() {
        String url_l = "http://65.1.160.150/api/master/foodtype";
        Log.i("url", url_l);
        StringRequest requestt = new StringRequest(url_l, new Response.Listener<String>() {
            @Override
            public void onResponse(String string) {
                try {

                    JSONObject jsonObject = new JSONObject(string);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");


                    for (int i = 0; i < jsonArray.length(); ++i) {

                        JSONObject c = jsonArray.getJSONObject(i);

                        String a = c.getString("id");
                        String b = c.getString("foodtype");


                        RegisterActive registerActive = new RegisterActive(a, b);
                        registerActives.add(registerActive);


                    }

                    RecyclerviewItemAdapter recyclerviewItemAdapter = new RecyclerviewItemAdapter(registerActives);
                    recyclerView.setAdapter(recyclerviewItemAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getApplicationContext(), "Check Your Network Connection", Toast.LENGTH_SHORT).show();

            }
        });

        RequestQueue rQueue = Volley.newRequestQueue(RegisterSixActivity.this);
        rQueue.add(requestt);
    }


}
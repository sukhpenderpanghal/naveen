package com.ennovations.fitcru.register;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;

import com.ennovations.fitcru.Api.ApiClient;
import com.ennovations.fitcru.Api.MyApiEndpointInterface;
import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.UpDateClientDataPojo;
import com.ennovations.fitcru.pojo.UpdateClientResponse;
import com.ennovations.fitcru.util.Common;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    public static final String MyPREFERENCES = "ActivityPREF";
    EditText number, email, f_name, l_name, age, pass;
    String _number, _email, _first_name, _last_name, _code, _contact, _name, _pas, token;
    TextView save;
    int agee, user_id;
    MyApiEndpointInterface myApiEndpointInterface;
    ProgressBar pb;
    AppCompatSpinner sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        sp = findViewById(R.id.spinner);
        number = findViewById(R.id.mobile_number);
        email = findViewById(R.id.email);
        f_name = findViewById(R.id.firsT_name);
        l_name = findViewById(R.id.last_name);
        age = findViewById(R.id.age);
        save = findViewById(R.id.txt_save);
        pass = findViewById(R.id.password);
        pb = findViewById(R.id.progressBarr);

        myApiEndpointInterface = ApiClient.getClient().create(MyApiEndpointInterface.class);
        token = Common.getToken(RegisterActivity.this);
        SharedPreferences prefs = getSharedPreferences("User_id", MODE_PRIVATE);
        user_id = prefs.getInt("user_id", 0);

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.country_code));
        sp.setAdapter(adapter);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _number = number.getText().toString();
                _contact = sp.getSelectedItem().toString() + _number;
                _pas = pass.getText().toString();
                _first_name = f_name.getText().toString();
                _last_name = l_name.getText().toString();
                _email = email.getText().toString();
                _name = _first_name + _last_name;

                if (number.getText().toString().isEmpty()) {
                    Common.toast(RegisterActivity.this, "Please enter mobile number");
                } else if (number.getText().length() != 10) {
                    Common.toast(RegisterActivity.this, "Please enter a valid mobile number");
                } else if (!Common.validEmail(_email)) {
                    Common.toast(RegisterActivity.this, "Email is not Valid");
                } else if (!Common.isValidPassword(_pas)) {
                    Common.toast(RegisterActivity.this, "Password is not Valid");
                } else if (_first_name.isEmpty()) {
                    Common.toast(RegisterActivity.this, "Please enter the first name");
                } else if (_last_name.isEmpty()) {
                    Common.toast(RegisterActivity.this, "Please enter the last name");
                } else if (agee != 0) {
                    Common.toast(RegisterActivity.this, "Please enter the Date of birth");
                } else {
                    pb.setVisibility(View.VISIBLE);
                    _contact = number.getText().toString();
                    _number = _code + _contact;

                    callUpDateClientApi();
                }
            }
        });
    }

    private void callUpDateClientApi() {

        UpDateClientDataPojo upDateClientDataPojo = new UpDateClientDataPojo();
        //upDateClientDataPojo.user_id = user_id;
        upDateClientDataPojo.name = (_first_name + " " + _last_name);
        upDateClientDataPojo.email = (email.toString());

        Call<UpdateClientResponse> call = myApiEndpointInterface.upDateClient("Bearer " + token, upDateClientDataPojo);
        call.enqueue(new Callback<UpdateClientResponse>() {
            @Override
            public void onResponse(Call<UpdateClientResponse> call, Response<UpdateClientResponse> response) {

                response.body();
                if (response.isSuccessful()) {
                    pb.setVisibility(View.GONE);
                    response.body();
                    Common.toast(RegisterActivity.this, response.body().message);
/*                    Intent intent = new Intent(RegisterActivity.this, RegistterOneActivity.class);
                    startActivity(intent);
                    finish();*/
                } else {
                    pb.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<UpdateClientResponse> call, Throwable t) {
                pb.setVisibility(View.GONE);
                call.cancel();
                t.getLocalizedMessage();
            }
        });
    }
}

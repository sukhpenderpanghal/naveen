package com.ennovations.fitcru.register;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.ennovations.fitcru.R;
import com.zjun.widget.RuleView;

public class RegistterOneActivity extends AppCompatActivity {

    TextView txt_female, txt_male, txt_save, txtHeight, txtWeight;
    RuleView ruleViewHeight, ruleViewWeight;
    ImageView back;
    int check = 0;
    int checkFemale = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registter_one);

        txt_female = findViewById(R.id.text_female);
        txt_male = findViewById(R.id.texT_male);
        back = findViewById(R.id.img_back_2);
        txt_save = findViewById(R.id.txt_save);
        txtHeight = findViewById(R.id.txt_height_inCM);
        txtWeight = findViewById(R.id.txt_weight_inKg);
        ruleViewHeight = findViewById(R.id.ruleViewHeight);
        ruleViewWeight = findViewById(R.id.ruleViewWeight);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegistterOneActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });
        ruleViewHeight.setOnValueChangedListener(new RuleView.OnValueChangedListener() {
            @Override
            public void onValueChanged(float value) {
                txtHeight.setText("Height" + " " + "(" + value + " " + "cm" + ")");
            }
        });

        ruleViewWeight.setOnValueChangedListener(new RuleView.OnValueChangedListener() {
            @Override
            public void onValueChanged(float value) {
                txtWeight.setText("Weight" + " " + "(" + value + " " + "kg" + ")");
            }
        });

        txt_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check == 0) {
                    txt_male.setTextColor(getResources().getColor(R.color.white));
                    txt_male.setBackgroundResource(R.drawable.custom_rounded_filled_black_color);
                    txt_female.setTextColor(getResources().getColor(R.color.black));
                    txt_female.setBackgroundResource(R.drawable.custom_rounded_filled_white_with_border_black_color);
                    check = 1;
                    checkFemale = 0;

                } else {
                    checkFemale = 1;
                    txt_female.setTextColor(getResources().getColor(R.color.white));
                    txt_female.setBackgroundResource(R.drawable.custom_rounded_filled_black_color);
                    txt_male.setTextColor(getResources().getColor(R.color.black));
                    txt_male.setBackgroundResource(R.drawable.custom_rounded_filled_white_with_border_black_color);
                    check = 0;
                }
            }
        });

        txt_female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkFemale == 0) {
                    check = 0;
                    txt_female.setTextColor(getResources().getColor(R.color.white));
                    txt_female.setBackgroundResource(R.drawable.custom_rounded_filled_black_color);
                    txt_male.setTextColor(getResources().getColor(R.color.black));
                    txt_male.setBackgroundResource(R.drawable.custom_rounded_filled_white_with_border_black_color);
                    checkFemale = 1;
                } else {
                    check = 1;
                    txt_male.setTextColor(getResources().getColor(R.color.white));
                    txt_male.setBackgroundResource(R.drawable.custom_rounded_filled_black_color);
                    txt_female.setTextColor(getResources().getColor(R.color.black));
                    txt_female.setBackgroundResource(R.drawable.custom_rounded_filled_white_with_border_black_color);
                    checkFemale = 0;
                }
            }
        });

        txt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(RegistterOneActivity.this, RegisterSixActivity.class);
                startActivity(intent);
                finish();

            }
        });


    }

}
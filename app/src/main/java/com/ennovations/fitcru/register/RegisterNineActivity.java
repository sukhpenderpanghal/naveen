package com.ennovations.fitcru.register;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ennovations.fitcru.DashboardActivity;
import com.ennovations.fitcru.R;
import com.ennovations.fitcru.WelcomeActivity;
import com.ennovations.fitcru.adapter.RecyclerviewItemAdapter;
import com.ennovations.fitcru.pojo.RegisterActive;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RegisterNineActivity extends AppCompatActivity {

    TextView save;
    RecyclerView recyclerView;
    RegisterActive registerActive;
    ArrayList<RegisterActive> registerActives = new ArrayList<>();
    RadioGroup radioGroup;
    RadioButton radioButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_nine);

        save = (TextView) findViewById(R.id.txt_save);
        radioGroup = (RadioGroup) findViewById(R.id.radio_group);

//        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
//
//        recyclerView.setHasFixedSize(true);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        getdata();


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int selectedId = radioGroup.getCheckedRadioButtonId();
                radioButton = (RadioButton) findViewById(selectedId);

                if (selectedId == -1)
                {
                    Toast.makeText(RegisterNineActivity.this,"Nothing selected, Please select", Toast.LENGTH_SHORT).show();
                }
                else {

                    Intent intent = new Intent(RegisterNineActivity.this, DashboardActivity.class);
                    startActivity(intent);
                }

            }
        });


    }

    public void getdata()
    {
        String url_l = "http://65.1.160.150/api/master/reasonchanges";
        Log.i("url",url_l);
        StringRequest requestt = new StringRequest(url_l, new Response.Listener<String>() {
            @Override
            public void onResponse(String string) {
                try
                {

                    JSONObject jsonObject = new JSONObject(string);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");


                    for(int i = 0; i < jsonArray.length(); ++i) {

                        JSONObject c = jsonArray.getJSONObject(i);

                        String a = c.getString("id");
                        String b = c.getString("value");


                        RegisterActive registerActive = new RegisterActive(a,b);
                        registerActives.add(registerActive);


                    }

                    RecyclerviewItemAdapter recyclerviewItemAdapter = new RecyclerviewItemAdapter(registerActives);
                    recyclerView.setAdapter(recyclerviewItemAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getApplicationContext(), "Check Your Network Connection", Toast.LENGTH_SHORT).show();

            }
        });

        RequestQueue rQueue = Volley.newRequestQueue(RegisterNineActivity.this);
        rQueue.add(requestt);
    }


}
package com.ennovations.fitcru.register;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.ennovations.fitcru.R;

public class RegisterThreeActivity extends AppCompatActivity {

    SharedPreferences sp;
    SharedPreferences.Editor editor;
    EditText weight;
    String _weight;
    TextView save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_three);

        sp = getSharedPreferences(RegisterActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sp.edit();

        weight = (EditText) findViewById(R.id.texT_weight);
        save = (TextView) findViewById(R.id.txt_save);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _weight = weight.getText().toString();

                editor.putString("weight",_weight);
                editor.commit();
                Intent intent = new Intent(RegisterThreeActivity.this,RegisterFourActivity.class);
                startActivity(intent);
            }
        });


    }
}
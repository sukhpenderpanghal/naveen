package com.ennovations.fitcru;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.ennovations.fitcru.register.RegisterActivity;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        Button btndash=findViewById(R.id.movetoDash);

        btndash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myfun();
            }
        });


    }

    private void myfun() {
        Intent intent = new Intent(WelcomeActivity.this, RegisterActivity.class);
        startActivity(intent);
    }


}
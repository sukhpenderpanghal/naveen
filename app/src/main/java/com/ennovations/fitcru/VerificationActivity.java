package com.ennovations.fitcru;

import static com.ennovations.fitcru.util.Common.hasInternetConnection;
import static com.ennovations.fitcru.util.Common.toast;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.ennovations.fitcru.Api.ApiClient;
import com.ennovations.fitcru.Api.MyApiEndpointInterface;
import com.ennovations.fitcru.pojo.EmailSignInResponse;
import com.ennovations.fitcru.util.Common;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.gne.www.lib.OnPinCompletedListener;
import com.gne.www.lib.PinView;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerificationActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final String EMAIL = "email";
    private static final int RC_SIGN_IN = 9001;
    TextView signin, resend, terms;
    String deviceName, m_number, otp = "";
    EditText _one, _two, _three, _four, _five, _six;
    ImageView image_google, image_facebook, image_email;
    LoginButton loginButton;
    CallbackManager callbackManager;
    GoogleApiClient mGoogleApiClient;
    MyApiEndpointInterface myApiEndpointInterface;
    ProgressBar pb;
    EditText e1,e2,e3,e4,e5,e6;
    //PinView pinView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        m_number = getIntent().getExtras().getString("mobile_number");
        signin = findViewById(R.id.text_signin);
        resend = findViewById(R.id.text_resend);

        image_google = findViewById(R.id.image_google);
        image_facebook = findViewById(R.id.image_facebook);
        image_email = findViewById(R.id.image_email);
        loginButton = findViewById(R.id.login_button);
        //pinView = findViewById(R.id.pinview);
        pb = findViewById(R.id.progressBarr);
        e1 = findViewById(R.id.edt_otp1);
        e2= findViewById(R.id.edt_otp2);
        e3 = findViewById(R.id.edt_otp3);
        e4 = findViewById(R.id.edt_otp4);
        e5 = findViewById(R.id.edt_otp5);
        e6 = findViewById(R.id.edt_otp6);

        myApiEndpointInterface = ApiClient.getClient().create(MyApiEndpointInterface.class);
        deviceName = Common.device_name();

//        FacebookSdk.sdkInitialize(getApplicationContext());

        /*pinView.setOnPinCompletionListener(new OnPinCompletedListener() {
            @Override
            public void onPinCompleted(String entirePin) {
                otp = entirePin;
            }
        });*/

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_clientid))
                .requestProfile()
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (e1.getText().toString().isEmpty()){
                    toast(VerificationActivity.this, "Please Fill Complete Otp");
                }else if (e2.getText().toString().isEmpty()){
                    toast(VerificationActivity.this, "Please Fill Complete Otp");
                }else if (e3.getText().toString().isEmpty()){
                    toast(VerificationActivity.this, "Please Fill Complete Otp");
                }else if (e4.getText().toString().isEmpty()){
                    toast(VerificationActivity.this, "Please Fill Complete Otp");
                }else if (e5.getText().toString().isEmpty()){
                    toast(VerificationActivity.this, "Please Fill Complete Otp");
                }else if (e6.getText().toString().isEmpty()){
                    toast(VerificationActivity.this, "Please Fill Complete Otp");
                }else {
                    String newOtp = (e1.getText().toString() +
                            e2.getText().toString() +
                            e3.getText().toString() +
                            e4.getText().toString() +
                            e5.getText().toString() +
                            e6.getText().toString());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (hasInternetConnection(VerificationActivity.this)){
                            pb.setVisibility(View.VISIBLE);
                            callVerifyOtpApi(newOtp, deviceName, m_number);
                        }else {
                            pb.setVisibility(View.GONE);
                            toast(VerificationActivity.this,"Please check your internet connection");
                        }
                    }
                }
            }
        });


        image_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VerificationActivity.this, EmailLoginActivity.class);
                startActivity(intent);
            }
        });

        image_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));
                callbackManager = CallbackManager.Factory.create();
                loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        //loginResult.getAccessToken();
                        //loginResult.getRecentlyDeniedPermissions()
                        //loginResult.getRecentlyGrantedPermissions()
                        boolean loggedIn = AccessToken.getCurrentAccessToken() == null;
                        Log.d("API123", loggedIn + " ??");

                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
            }
        });

        image_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInClick();

            }
        });


    }

    private void callVerifyOtpApi(String otp, String d_name, String mobile) {

        Call<EmailSignInResponse> call1 = myApiEndpointInterface.verifyOtp(otp, d_name, mobile);

        call1.enqueue(new Callback<EmailSignInResponse>() {
            @Override
            public void onResponse(Call<EmailSignInResponse> call, Response<EmailSignInResponse> response) {

                EmailSignInResponse emailSignInDataClass = response.body();

                pb.setVisibility(View.GONE);
                Common.toast(VerificationActivity.this, response.message());

                if (response.body() != null) {
                    if (response.body().message.equals("success") && response.body().status == 200) {

                        Common.setToken(VerificationActivity.this, response.body().token);
                        SharedPreferences.Editor editor = getSharedPreferences("User_id", MODE_PRIVATE).edit();
                        editor.putInt("user_id", response.body().user.id);
                        editor.apply();

                        startActivity(new Intent(VerificationActivity.this, WelcomeActivity.class));
                        finish();
                    } else {
                        Common.toast(VerificationActivity.this, response.body().message);
                    }
                } else {
                    Common.toast(VerificationActivity.this, response.body().message);
                }
            }

            @Override
            public void onFailure(Call<EmailSignInResponse> call, Throwable t) {
                pb.setVisibility(View.GONE);
                call.cancel();
                Common.toast(VerificationActivity.this, call.toString());
            }
        });
    }


    private void signInClick() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);

        }

    }

    private void getUserProfile(AccessToken currentAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                currentAccessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.d("TAG", object.toString());
                        try {
                            String first_name = object.getString("first_name");
                            String last_name = object.getString("last_name");
                            String email = object.getString("email");
                            String id = object.getString("id");
                            String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";

//                            txtUsername.setText("First Name: " + first_name + "\nLast Name: " + last_name);
//                            txtEmail.setText(email);
//                            Picasso.with(EmailLoginActivity.this).load(image_url).into(imageView);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();

    }


    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String idToken = acct.getIdToken();
            Log.d("DD", idToken);
//            signout.setVisibility(View.VISIBLE);
//            signin.setVisibility(View.GONE);

        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}

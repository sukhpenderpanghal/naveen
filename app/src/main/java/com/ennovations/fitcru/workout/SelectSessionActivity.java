package com.ennovations.fitcru.workout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.SessionPojo;
import com.ennovations.fitcru.pojo.StudioPojo;
import com.ennovations.fitcru.workout.adapter.RecyclerviewSessionAdapter;
import com.ennovations.fitcru.workout.adapter.RecyclerviewStudioAdapter;

import java.util.ArrayList;

public class SelectSessionActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<SessionPojo> sessionPojoArrayList = new ArrayList<>();
    RecyclerviewSessionAdapter recyclerviewSessionAdapter;
    TextView save;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_session);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        save =  (TextView) findViewById(R.id.text_save);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        back = (ImageView) findViewById(R.id.image_backpressed);

        sessionPojoArrayList.add(new SessionPojo("1","Studio Name",R.drawable.image_plan,"14 Exercise, 200 mins, 500 claries burn","Not Started"));
        sessionPojoArrayList.add(new SessionPojo("1","Studio Name",R.drawable.image_plan,"14 Exercise, 200 mins, 500 claries burn","Not Started"));
        sessionPojoArrayList.add(new SessionPojo("1","Studio Name",R.drawable.image_plan,"14 Exercise, 200 mins, 500 claries burn","Not Started"));
        sessionPojoArrayList.add(new SessionPojo("1","Studio Name",R.drawable.image_plan,"14 Exercise, 200 mins, 500 claries burn","Not Started"));
        sessionPojoArrayList.add(new SessionPojo("1","Studio Name",R.drawable.image_plan,"14 Exercise, 200 mins, 500 claries burn","Not Started"));
        sessionPojoArrayList.add(new SessionPojo("1","Studio Name",R.drawable.image_plan,"14 Exercise, 200 mins, 500 claries burn","Not Started"));

        recyclerviewSessionAdapter = new RecyclerviewSessionAdapter(sessionPojoArrayList);
        recyclerView.setAdapter(recyclerviewSessionAdapter);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectSessionActivity.this,ExploreExerciseActivity.class);
                startActivity(intent);
                finish();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
}
package com.ennovations.fitcru.workout.plan;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ennovations.fitcru.Api.ApiClient;
import com.ennovations.fitcru.Api.MyApiEndpointInterface;
import com.ennovations.fitcru.ItemClickListener;
import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.WorkOutPlansPojo;
import com.ennovations.fitcru.util.Common;
import com.ennovations.fitcru.workout.plan.adapter.RecyclerviewPlanAdapter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PlansFragment extends Fragment implements ItemClickListener {

    RecyclerView recyclerView;
    RecyclerviewPlanAdapter recyclerviewPlanAdapter;
    MyApiEndpointInterface myApiEndpointInterface;
    ProgressBar pb;

    public PlansFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_plans, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        pb = view.findViewById(R.id.progressBarr);
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        myApiEndpointInterface = ApiClient.getClient().create(MyApiEndpointInterface.class);

        pb.setVisibility(View.VISIBLE);
        callPlanListApi();
    }

    private void callPlanListApi() {
        myApiEndpointInterface.getWorkOutPlans().enqueue(new Callback<WorkOutPlansPojo>() {
            @Override
            public void onResponse(Call<WorkOutPlansPojo> call, Response<WorkOutPlansPojo> response) {
                pb.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body() != null) {
                    recyclerviewPlanAdapter = new RecyclerviewPlanAdapter(response.body().data, getContext(), PlansFragment.this);
                    recyclerView.setAdapter(recyclerviewPlanAdapter);
                } else {
                }
            }

            @Override
            public void onFailure(Call<WorkOutPlansPojo> call, Throwable t) {
                pb.setVisibility(View.GONE);
                call.cancel();
                Common.toast(getContext(), t.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onAdapterItemClickListener(int id) {
        Intent intent = new Intent(getContext(), PlanDetailActivity.class);
        intent.putExtra("PlanId", id);
        startActivity(intent);
    }
}
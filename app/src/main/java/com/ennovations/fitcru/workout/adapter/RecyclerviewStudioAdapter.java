package com.ennovations.fitcru.workout.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.Workout;

import java.util.List;

public class RecyclerviewStudioAdapter extends RecyclerView.Adapter<RecyclerviewStudioAdapter.MyViewHolder> {

    private final List<Workout> itemsList;
    int selectedPosition = -1;
    Context context;

    public RecyclerviewStudioAdapter(List<Workout> mItemList, Context context) {
        this.itemsList = mItemList;
        this.context = context;
    }

    @Override
    public RecyclerviewStudioAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_studio_recyclerview, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final Workout item = itemsList.get(position);
        holder.studio_name.setText(item.seesion_name);
        Glide.with(context).load(item.banner).into(holder.banner_image);

        if (selectedPosition == position)
            holder.img_green_check.setVisibility(View.VISIBLE);
        else
            holder.img_green_check.setVisibility(View.GONE);
        holder.itemView.setOnClickListener(v -> {
            selectedPosition = holder.getAdapterPosition();
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout relativeLayout;
        TextView studio_name;
        ImageView img_green_check, banner_image;

        public MyViewHolder(View itemView) {
            super(itemView);
            relativeLayout = itemView.findViewById(R.id.relativelayout);
            studio_name = itemView.findViewById(R.id.studio_name);
            img_green_check = itemView.findViewById(R.id.img_green_check);
            banner_image = itemView.findViewById(R.id.img_session_image);
        }
    }
}

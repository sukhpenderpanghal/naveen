package com.ennovations.fitcru.workout.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.StudioPojo;
import com.ennovations.fitcru.pojo.WorkoutDetailPojo;

import java.util.List;

public class RecyclerviewWorkoutDetailAdapter extends RecyclerView.Adapter<RecyclerviewWorkoutDetailAdapter.MyViewHolder> {

    private List<WorkoutDetailPojo> itemsList;

    public RecyclerviewWorkoutDetailAdapter(List<WorkoutDetailPojo> mItemList){
        this.itemsList = mItemList;
    }

    @Override
    public RecyclerviewWorkoutDetailAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_recyclerview_workoutdetail,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final WorkoutDetailPojo item = itemsList.get(position);
        holder.workout_name.setText(item.getWorkout_name());


    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView time, workout_name, workout_detail, workout_reps, workout_weight, txt_clock;

        public MyViewHolder(View itemView) {
            super(itemView);
            time = (TextView) itemView.findViewById(R.id.text_time);
            workout_name = (TextView) itemView.findViewById(R.id.workout_name);
            workout_detail = (TextView) itemView.findViewById(R.id.workout_detail);
            workout_reps = (TextView) itemView.findViewById(R.id.workout_reps);
            workout_weight = (TextView) itemView.findViewById(R.id.workout_detail);
            txt_clock = (TextView) itemView.findViewById(R.id.clock_time);

        }
    }
}

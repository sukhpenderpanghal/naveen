package com.ennovations.fitcru.workout.plan.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ennovations.fitcru.ItemClickListener;
import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.PlanDetails;

import java.util.List;

public class BuyPlansDetailAdapter extends RecyclerView.Adapter<BuyPlansDetailAdapter.MyViewHolder> {

    private final List<PlanDetails> itemsList;
    private ItemClickListener itemClickListener = null;

    public BuyPlansDetailAdapter(List<PlanDetails> mItemList) {
        this.itemsList = mItemList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public BuyPlansDetailAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.buy_plan_item_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final PlanDetails item = itemsList.get(position);
        holder.plan_type.setText(item.getTime_interval());
        holder.plan_cost.setText(item.getCost());
        //holder.plan_days.setText(item.getDuration());

        holder.buy_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //itemClickListener.onAdapterItemClickListener(item.id);
            }
        });

    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView plan_type, plan_cost, plan_days, buy_plan;

        public MyViewHolder(View itemView) {
            super(itemView);
            plan_type = (TextView) itemView.findViewById(R.id.txt_plan_type);
            plan_days = (TextView) itemView.findViewById(R.id.txt_plan_days);
            plan_cost = itemView.findViewById(R.id.txt_plan_cost);
            buy_plan = itemView.findViewById(R.id.txt_buy_plan);
        }
    }
}

package com.ennovations.fitcru.workout.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.ExploreExercisePojo;

import java.util.List;

public class RecyclerViewExploreExerciseAdapter extends RecyclerView.Adapter<RecyclerViewExploreExerciseAdapter.MyViewHolder> {

    private final List<ExploreExercisePojo> itemsList;

    public RecyclerViewExploreExerciseAdapter(List<ExploreExercisePojo> mItemList) {
        this.itemsList = mItemList;
    }

    @Override
    public RecyclerViewExploreExerciseAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_explore_exercise, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ExploreExercisePojo item = itemsList.get(position);
        holder.explore_exer_name.setText(item.getWorkout_name());
        holder.explore_exercise_image.setImageResource(item.getImage());
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView explore_exercise_image;
        TextView explore_exer_name;

        public MyViewHolder(View itemView) {
            super(itemView);
            explore_exercise_image = (ImageView) itemView.findViewById(R.id.img_explore_exercise);
            explore_exer_name = (TextView) itemView.findViewById(R.id.txt_explore_exercise_name);
        }
    }
}


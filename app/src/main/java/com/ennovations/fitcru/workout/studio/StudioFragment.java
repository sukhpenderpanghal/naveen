package com.ennovations.fitcru.workout.studio;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.ennovations.fitcru.Api.ApiClient;
import com.ennovations.fitcru.Api.MyApiEndpointInterface;
import com.ennovations.fitcru.ItemClickListener;
import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.StudioPojo;
import com.ennovations.fitcru.pojo.StudioResponsePojo;
import com.ennovations.fitcru.pojo.WorkOutPlansPojo;
import com.ennovations.fitcru.util.Common;
import com.ennovations.fitcru.workout.SelectStudioActivity;
import com.ennovations.fitcru.workout.adapter.RecyclerviewStudioAdapter;
import com.ennovations.fitcru.workout.plan.PlansFragment;
import com.ennovations.fitcru.workout.plan.adapter.RecyclerviewPlanAdapter;
import com.ennovations.fitcru.workout.studio.adapter.StudioMainListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class StudioFragment extends Fragment implements ItemClickListener {

    RecyclerView recyclerView;
    ArrayList<StudioPojo> studioPojoArrayList = new ArrayList<>();
    StudioMainListAdapter studioMainListAdapter;
    MyApiEndpointInterface myApiEndpointInterface;
    ProgressBar pb;
    ImageView main_image;
    String token;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_studio, container, false);

        main_image = view.findViewById(R.id.banner_studio);
        pb = view.findViewById(R.id.progressBarr);

        //getdata();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_studio);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        myApiEndpointInterface = ApiClient.getClient().create(MyApiEndpointInterface.class);

        token = Common.getToken(getContext());
        pb.setVisibility(View.VISIBLE);
        callStudioApi();
    }

    private void callStudioApi() {
        myApiEndpointInterface.getStudiosList("Bearer " + "142|1VWFImtOUmXl78S9psdBpouZ8TR5UB35h81IjCpH").enqueue(new Callback<StudioResponsePojo>() {
            @Override
            public void onResponse(Call<StudioResponsePojo> call, retrofit2.Response<StudioResponsePojo> response) {
                pb.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.isSuccessful()) {
                        Glide.with(getContext()).load(response.body().main_banner).into(main_image);
                        studioMainListAdapter = new StudioMainListAdapter(response.body().data, (ItemClickListener) StudioFragment.this);
                        recyclerView.setAdapter(studioMainListAdapter);

                    } else {
                        Common.toast(getContext(), response.body().message);
                    }
                }
            }

            @Override
            public void onFailure(Call<StudioResponsePojo> call, Throwable t) {
                pb.setVisibility(View.GONE);
                call.cancel();
                Common.toast(getContext(), t.getLocalizedMessage());
            }
        });
    }

    public void getdata()
    {
        String url_l = "http://65.1.160.150/api/master/studio";
        Log.i("url",url_l);
        StringRequest requestt = new StringRequest(url_l, new Response.Listener<String>() {
            @Override
            public void onResponse(String string) {
                try
                {

                    JSONObject jsonObject = new JSONObject(string);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");


                    for(int i = 0; i < jsonArray.length(); ++i) {

                        JSONObject c = jsonArray.getJSONObject(i);

                        String a = c.getString("id");
                        String b = c.getString("banner");
                        String d = c.getString("title");
                        String e = c.getString("description");


                        //StudioPojo studioPojo = new StudioPojo(a,d,b);
                        //studioPojoArrayList.add(studioPojo);

                    }

                    //recyclerviewStudioAdapter = new RecyclerviewStudioAdapter(studioPojoArrayList);
                    //recyclerView.setAdapter(recyclerviewStudioAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getContext(), "Check Your Network Connection", Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Authorization","Bearer 142|1VWFImtOUmXl78S9psdBpouZ8TR5UB35h81IjCpH");
                return params;
            }
        };

        RequestQueue rQueue = Volley.newRequestQueue(getContext());
        rQueue.add(requestt);
    }

    @Override
    public void onAdapterItemClickListener(int id) {
        Intent i = new Intent(getContext(), SelectStudioActivity.class);
        i.putExtra("studio_Id",id);
        startActivity(i);
    }
}
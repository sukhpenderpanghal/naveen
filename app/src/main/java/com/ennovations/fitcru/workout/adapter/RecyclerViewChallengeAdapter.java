package com.ennovations.fitcru.workout.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.ChallengeCurrent;

import java.util.List;

public class RecyclerViewChallengeAdapter extends RecyclerView.Adapter<RecyclerViewChallengeAdapter.MyViewHolder> {

    private final List<ChallengeCurrent> itemsList;
    Context context;

    public RecyclerViewChallengeAdapter(List<ChallengeCurrent> mItemList, Context context) {
        this.itemsList = mItemList;
        this.context = context;
    }

    @Override
    public RecyclerViewChallengeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_challenge, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ChallengeCurrent item = itemsList.get(position);

        Glide.with(context).load(item.banner).into(holder.challenge_image);
        holder.challenge_title.setText(item.title);
        holder.txt_challenge_description.setText(item.description);
        holder.txt_challenge_days_left.setText(item.duration);
        holder.txt_price.setText(item.price);
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView challenge_image;
        TextView challenge_title,
                txt_price,
                txt_challenge_description,
                txt_challenge_days_left;

        public MyViewHolder(View itemView) {
            super(itemView);
            challenge_image = itemView.findViewById(R.id.img_challenge_workout);
            challenge_title = itemView.findViewById(R.id.txt_challenge_title);
            txt_price = itemView.findViewById(R.id.txt_challenge_price);
            txt_challenge_description = itemView.findViewById(R.id.txt_challenge_description);
            txt_challenge_days_left = itemView.findViewById(R.id.txt_challenge_days_left);
        }
    }
}


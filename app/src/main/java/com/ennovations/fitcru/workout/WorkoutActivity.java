package com.ennovations.fitcru.workout;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.ennovations.fitcru.R;
import com.ennovations.fitcru.workout.adapter.MyAdapter;
import com.google.android.material.tabs.TabLayout;

public class WorkoutActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ImageView image_backpress, edit_workout;
    ViewPager viewPager;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout);

        tabLayout = (TabLayout) findViewById(R.id.simpleTabLayout);
        image_backpress = (ImageView) findViewById(R.id.image_backpressed);
        edit_workout = (ImageView) findViewById(R.id.image_edit_workout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        textView = (TextView) findViewById(R.id.text);

        textView.setText(R.string.lbl_workout);

        tabLayout.addTab(tabLayout.newTab().setText(R.string.lbl_work_plan));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.lbl_work_studio));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.lbl_work_chanllenge));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.lbl_today_Session));
        tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);


        final MyAdapter adapter = new MyAdapter(this, getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 3) edit_workout.setVisibility(View.VISIBLE);
                else edit_workout.setVisibility(View.GONE);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        edit_workout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(WorkoutActivity.this);
                dialog.setContentView(R.layout.dialog_explore);
                dialog.setTitle("Custom Dialog");

                TextView select_studio = (TextView) dialog.findViewById(R.id.select_studio);
                TextView select_excersice = (TextView) dialog.findViewById(R.id.select_excersice);

                dialog.show();

                select_studio.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(WorkoutActivity.this, SelectStudioActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });

                select_excersice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(WorkoutActivity.this, ExploreExerciseActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
            }
        });
    }
}
package com.ennovations.fitcru.workout.todaySession;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ennovations.fitcru.Api.ApiClient;
import com.ennovations.fitcru.Api.MyApiEndpointInterface;
import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.TodaySessionCoreExercisePojo;
import com.ennovations.fitcru.pojo.TodaySessionResponse;
import com.ennovations.fitcru.util.Common;
import com.ennovations.fitcru.workout.TodaySessionStartWorkOutActivity;
import com.ennovations.fitcru.workout.adapter.TodaySessionCoreWorkOutAdapter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TodaySessionFragment extends Fragment {

    TextView textView_start, session_name,avg_min,intensity_lvl, level, txt_benefits;
    RecyclerView recyclerView;
    String token;
    TodaySessionCoreWorkOutAdapter todaySessionCoreWorkOutAdapter;
    MyApiEndpointInterface myApiEndpointInterface;
    ProgressBar pb;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_today_session, container, false);

        textView_start = view.findViewById(R.id.txt_start);
        avg_min = view.findViewById(R.id.minute_count);
        intensity_lvl = view.findViewById(R.id.intensity_today);
        session_name = view.findViewById(R.id.name_session);
        level = view.findViewById(R.id.beginner_level);
        txt_benefits = view.findViewById(R.id.txt_benefits);
        pb = view.findViewById(R.id.progressBarr);

        token = Common.getToken(getContext());

        textView_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), TodaySessionStartWorkOutActivity.class);
                startActivity(intent);

       /*         final Dialog dialog = new Dialog(getContext());
                dialog.setContentView(R.layout.dialog_explore);
                dialog.setTitle("Custom Dialog");

                TextView select_studio = (TextView) dialog.findViewById(R.id.select_studio);
                TextView select_excersice = (TextView) dialog.findViewById(R.id.select_excersice);

                dialog.show();

                select_studio.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext(), SelectStudioActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });

                select_excersice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext(), ExploreExerciseActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });*/

            }
        });

        recyclerView = view.findViewById(R.id.rv_coreWorkOut);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

       // todaySessionCoreWorkOutAdapter = new TodaySessionCoreWorkOutAdapter(coreWorkoutList);
       // recyclerView.setAdapter(todaySessionCoreWorkOutAdapter);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        myApiEndpointInterface = ApiClient.getClient().create(MyApiEndpointInterface.class);

        pb.setVisibility(View.VISIBLE);
        callTodaySessionApi();
    }

    private void callTodaySessionApi() {
        myApiEndpointInterface.getTodaySession("Bearer " + "142|1VWFImtOUmXl78S9psdBpouZ8TR5UB35h81IjCpH").enqueue(new Callback<TodaySessionResponse>() {
            @Override
            public void onResponse(Call<TodaySessionResponse> call, Response<TodaySessionResponse> response) {
                pb.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.isSuccessful()) {
                        upDateUi(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<TodaySessionResponse> call, Throwable t) {
                pb.setVisibility(View.GONE);
                Common.toast(getContext(), t.getLocalizedMessage());
            }
        });
    }

    private void upDateUi(TodaySessionResponse body) {
        session_name.setText(body.session_name);
        avg_min.setText(body.avg_mins);
        intensity_lvl.setText(body.intensity);
        level.setText(body.level);
        txt_benefits.setText(body.benefits);

        todaySessionCoreWorkOutAdapter = new TodaySessionCoreWorkOutAdapter(body.exercise_plan);
        recyclerView.setAdapter(todaySessionCoreWorkOutAdapter);
    }
}

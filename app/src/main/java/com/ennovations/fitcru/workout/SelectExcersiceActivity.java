package com.ennovations.fitcru.workout;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ennovations.fitcru.R;

public class SelectExcersiceActivity extends AppCompatActivity {

    ImageView back;
    TextView save;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_excersice);

        back = (ImageView) findViewById(R.id.image_backpressed);
        save =  (TextView) findViewById(R.id.text_save);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectExcersiceActivity.this,SelectExcersiceActivity.class);
                startActivity(intent);
            }
        });

    }
}




















































































































































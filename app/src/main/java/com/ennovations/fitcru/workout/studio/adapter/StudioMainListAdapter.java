package com.ennovations.fitcru.workout.studio.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ennovations.fitcru.ItemClickListener;
import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.PlanDetails;
import com.ennovations.fitcru.pojo.StudioData;

import java.util.ArrayList;
import java.util.List;

public class StudioMainListAdapter extends RecyclerView.Adapter<StudioMainListAdapter.MyViewHolder> {

    private final List<StudioData> itemsList;
    private ItemClickListener itemClickListener = null;

    public StudioMainListAdapter(ArrayList<StudioData> mItemList, com.ennovations.fitcru.ItemClickListener itemClickListener) {
        this.itemsList = mItemList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public StudioMainListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_studio_layout
                , parent, false);
        return new StudioMainListAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StudioMainListAdapter.MyViewHolder holder, final int position) {
        final StudioData item = itemsList.get(position);
        holder.studio_name.setText(item.title);
        Glide.with(holder.itemView).load(item.banner).into(holder.img_studio);
        //holder.plan_days.setText(item.getDuration());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onAdapterItemClickListener(item.id);
            }
        });

    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView studio_name;
        ImageView img_studio;

        public MyViewHolder(View itemView) {
            super(itemView);
            studio_name = (TextView) itemView.findViewById(R.id.txt_studio_main_name);
            img_studio = itemView.findViewById(R.id.img_studio);

        }
    }
}

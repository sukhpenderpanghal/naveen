package com.ennovations.fitcru.workout.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.SessionPojo;
import com.ennovations.fitcru.pojo.StudioPojo;

import java.util.List;

public class RecyclerviewSessionAdapter extends RecyclerView.Adapter<RecyclerviewSessionAdapter.MyViewHolder> {

    private List<SessionPojo> itemsList;

    public RecyclerviewSessionAdapter(List<SessionPojo> mItemList){
        this.itemsList = mItemList;
    }

    @Override
    public RecyclerviewSessionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_session_selection,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final SessionPojo item = itemsList.get(position);
        holder.workout_name.setText(item.getName());
        holder.work_Detail.setText(item.getDescription());
        holder.workout_time.setText(item.getData());


    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        RelativeLayout relativeLayout;
        TextView workout_name, work_Detail, workout_time;

        public MyViewHolder(View itemView) {
            super(itemView);
            relativeLayout = itemView.findViewById(R.id.relativelayout);
            workout_name = (TextView) itemView.findViewById(R.id.workout_name);
            work_Detail = (TextView) itemView.findViewById(R.id.workout_detail);
            workout_time = (TextView) itemView.findViewById(R.id.workout_time);
        }
    }
}

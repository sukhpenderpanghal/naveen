package com.ennovations.fitcru.workout.adapter;

import android.media.MediaPlayer;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.ExercisePlan;
import com.ennovations.fitcru.pojo.TodaySessionCoreExercisePojo;

import java.util.ArrayList;
import java.util.List;

public class TodaySessionCoreWorkOutAdapter extends RecyclerView.Adapter<TodaySessionCoreWorkOutAdapter.MyViewHolder> {

    private final List<ExercisePlan> itemsList;
    private boolean check = true;

    public TodaySessionCoreWorkOutAdapter(ArrayList<ExercisePlan> mItemList) {
        this.itemsList = mItemList;
    }

    @Override
    public TodaySessionCoreWorkOutAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_today_session_core_workout_, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ExercisePlan item = itemsList.get(position);

        holder.workout_name.setText(item.title);
        holder.time.setText(item.target);
        Glide.with(holder.itemView).load(item.banner).into(holder.workout_image);

        holder.Play_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.video_url.start();
                holder.Play_video.setVisibility(View.GONE);
            }
        });

        holder.video_url.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                holder.Play_video.setVisibility(View.VISIBLE);
            }
        });


        holder.collapsing_arrow_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check){
                    if (item.video != null){
                        holder.no_data_found.setVisibility(View.GONE);
                        holder.video_url.setVisibility(View.VISIBLE);
                        holder.Play_video.setVisibility(View.VISIBLE);
                        Uri myUri = Uri.parse(Uri.decode(item.video));
                        holder.video_url.setVideoURI(myUri);
                    }else {
                        holder.no_data_found.setVisibility(View.VISIBLE);
                        holder.video_url.setVisibility(View.GONE);
                        holder.Play_video.setVisibility(View.GONE);
                    }
                    check = false;
                    holder.collapsing_arrow_image.setImageResource(R.drawable.ic_baseline_keyboard_arrow_down_24);
                }
                else{
                    check = true;
                    holder.video_url.setVisibility(View.GONE);
                    holder.Play_video.setVisibility(View.GONE);
                    holder.no_data_found.setVisibility(View.GONE);
                    holder.video_url.stopPlayback();
                    holder.collapsing_arrow_image.setImageResource(R.drawable.ic_baseline_keyboard_arrow_up_24);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView workout_image,collapsing_arrow_image, Play_video;
        TextView time, workout_name,no_data_found;
        VideoView video_url;

        public MyViewHolder(View itemView) {
            super(itemView);
            workout_image = (ImageView) itemView.findViewById(R.id.img_core);
            Play_video = (ImageView) itemView.findViewById(R.id.play_video);
            collapsing_arrow_image = (ImageView) itemView.findViewById(R.id.img_workout_collapse_arrow);
            time = (TextView) itemView.findViewById(R.id.txt_core_time);
            workout_name = (TextView) itemView.findViewById(R.id.txt_core_name);
            no_data_found = (TextView) itemView.findViewById(R.id.txt_no_video_found);
            video_url = itemView.findViewById(R.id.video_view_today_session);

        }
    }
}

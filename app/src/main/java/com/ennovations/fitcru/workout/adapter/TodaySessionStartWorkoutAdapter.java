package com.ennovations.fitcru.workout.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.TodaySessionStartWorkoutPojo;

import java.util.List;

public class TodaySessionStartWorkoutAdapter extends RecyclerView.Adapter<TodaySessionStartWorkoutAdapter.MyViewHolder> {

    private final List<TodaySessionStartWorkoutPojo> itemsList;
    private final boolean check = true;

    public TodaySessionStartWorkoutAdapter(List<TodaySessionStartWorkoutPojo> mItemList) {
        this.itemsList = mItemList;
    }

    @Override
    public TodaySessionStartWorkoutAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_today_session_start_workout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final TodaySessionStartWorkoutPojo item = itemsList.get(position);
        if (position%2 == 0){
            holder.lin_lay.setBackgroundResource(R.color.light_grey);
        }else {
            holder.lin_lay.setBackgroundResource(R.color.white);

        }
        holder.workout_name.setText(item.getWorkOutName());
        holder.workout_time.setText(item.getTime());
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView workout_time, workout_name;
        LinearLayout lin_lay;

        public MyViewHolder(View itemView) {
            super(itemView);
            workout_time = (TextView) itemView.findViewById(R.id.txt_workout_time);
            workout_name = (TextView) itemView.findViewById(R.id.txt_workout_name);
            lin_lay = (LinearLayout) itemView.findViewById(R.id.lin_lay_workout_start);

        }
    }
}

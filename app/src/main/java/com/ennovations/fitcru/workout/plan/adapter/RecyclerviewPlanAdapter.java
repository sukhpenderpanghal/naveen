package com.ennovations.fitcru.workout.plan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.PlanData;

import java.util.List;

public class RecyclerviewPlanAdapter extends RecyclerView.Adapter<RecyclerviewPlanAdapter.MyViewHolder> {

    private final List<PlanData> itemsList;
    private final com.ennovations.fitcru.ItemClickListener itemClickListener;
    Context context;

    public RecyclerviewPlanAdapter(List<PlanData> mItemList, Context context, com.ennovations.fitcru.ItemClickListener itemClickListener) {
        this.itemsList = mItemList;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public RecyclerviewPlanAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_plan_recyclerview, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final PlanData item = itemsList.get(position);
        holder.name.setText(item.getTitle());
        holder.description.setText(item.getDescription());
        Glide.with(context)
                .load(item.banner)
                .into(holder.plan_img);

        holder.txt_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onAdapterItemClickListener(item.id);
            }
        });

    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name, description;
        Button txt_buy;
        ImageView plan_img;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.plan_name);
            description = (TextView) itemView.findViewById(R.id.plan_description);
            txt_buy = itemView.findViewById(R.id.text_buy);
            plan_img = itemView.findViewById(R.id.img_plan_list_banner);
        }
    }
}

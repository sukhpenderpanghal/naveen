package com.ennovations.fitcru.workout.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.PlanDetailFeedbackPojo;
import com.ennovations.fitcru.pojo.PlanDetailTextPojo;

import java.util.List;

public class RecyclerviewPlanDetailFeedbackAdapter extends RecyclerView.Adapter<RecyclerviewPlanDetailFeedbackAdapter.MyViewHolder> {

    private List<PlanDetailFeedbackPojo> itemsList;
    Context context;

    public RecyclerviewPlanDetailFeedbackAdapter(List<PlanDetailFeedbackPojo> mItemList, Context context){
        this.itemsList = mItemList;
        this.context = context;
    }

    @Override
    public RecyclerviewPlanDetailFeedbackAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_recycler_text,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final PlanDetailFeedbackPojo item = itemsList.get(position);
        holder.recycler_text.setText(item.getDescription());


    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        RelativeLayout relativeLayout;
        TextView recycler_text;

        public MyViewHolder(View itemView) {
            super(itemView);
            recycler_text = (TextView) itemView.findViewById(R.id.recycler_text);
        }
    }
}

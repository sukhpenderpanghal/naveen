package com.ennovations.fitcru.workout.plan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ennovations.fitcru.Api.ApiClient;
import com.ennovations.fitcru.Api.MyApiEndpointInterface;
import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.WorkOutPlanDetailsPojo;
import com.ennovations.fitcru.util.Common;
import com.ennovations.fitcru.workout.plan.adapter.BuyPlansDetailAdapter;
import com.ennovations.fitcru.workout.plan.adapter.RecyclerviewPlanAdapter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlanDetailActivity extends AppCompatActivity {

    ImageView imageView, image_back;
    RecyclerView recyclerView, recyclerView_text, recycler_feedback;
    LinearLayout layout_buy;
    TextView textView,planDetailTitle,planDetailDescription;
    int id;
    MyApiEndpointInterface myApiEndpointInterface;
    ProgressBar pb;
    BuyPlansDetailAdapter buyPlansDetailAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_detail);

        id = getIntent().getExtras().getInt("PlanId");

        image_back = (ImageView) findViewById(R.id.image_backpressed);
        imageView = (ImageView) findViewById(R.id.img_planDetails);
        recycler_feedback = (RecyclerView) findViewById(R.id.recycler_feedback);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView_text = (RecyclerView) findViewById(R.id.recyclerview_text);
        layout_buy = (LinearLayout) findViewById(R.id.linear_buy);
        textView = (TextView) findViewById(R.id.text);
        planDetailTitle = findViewById(R.id.txt_planDetail_title);
        planDetailDescription = findViewById(R.id.txt_planDetail_description);
        pb = findViewById(R.id.progressBarr);
        recyclerView = (RecyclerView) findViewById(R.id.rv_buy_plan);

        myApiEndpointInterface = ApiClient.getClient().create(MyApiEndpointInterface.class);
        pb.setVisibility(View.VISIBLE);
        callPlanDetailApi();
        textView.setText(R.string.lbl_plan_detail);

        image_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void callPlanDetailApi() {
        myApiEndpointInterface.getParticularPlanDetails(id).enqueue(new Callback<WorkOutPlanDetailsPojo>() {
            @Override
            public void onResponse(Call<WorkOutPlanDetailsPojo> call, Response<WorkOutPlanDetailsPojo> response) {
                pb.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body() != null){
                    setData(response.body());
                    buyPlansDetailAdapter = new BuyPlansDetailAdapter(response.body().plans);
                    recyclerView.setAdapter(buyPlansDetailAdapter);
                }else {
                    Common.toast(PlanDetailActivity.this,"No Data Found");
                }
            }

            @Override
            public void onFailure(Call<WorkOutPlanDetailsPojo> call, Throwable t) {
                pb.setVisibility(View.GONE);
                Common.toast(PlanDetailActivity.this,t.getLocalizedMessage());

            }
        });
    }

    private void setData(WorkOutPlanDetailsPojo body) {
        planDetailTitle.setText(body.title);
        planDetailDescription.setText(body.description);
        Glide.with(PlanDetailActivity.this).load(body.banner).into(imageView);
    }
}
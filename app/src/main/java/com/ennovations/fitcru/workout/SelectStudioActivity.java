package com.ennovations.fitcru.workout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.ennovations.fitcru.Api.ApiClient;
import com.ennovations.fitcru.Api.MyApiEndpointInterface;
import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.SingleStudioResponse;
import com.ennovations.fitcru.pojo.StudioId;
import com.ennovations.fitcru.util.Common;
import com.ennovations.fitcru.workout.adapter.RecyclerviewStudioAdapter;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;

public class SelectStudioActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerviewStudioAdapter recyclerviewStudioAdapter;
    TextView save;
    ImageView back;
    int studio_Id;
    MyApiEndpointInterface myApiEndpointInterface;
    ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_studio);

        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        save = findViewById(R.id.text_save);
        back = findViewById(R.id.image_backpressed);
        pb = findViewById(R.id.progressBarr);

        myApiEndpointInterface = ApiClient.getClient().create(MyApiEndpointInterface.class);
        studio_Id = getIntent().getExtras().getInt("studio_Id");

        pb.setVisibility(View.VISIBLE);
        callParticularStudioApi();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectStudioActivity.this, SelectSessionActivity.class);
                startActivity(intent);
                finish();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void callParticularStudioApi() {
        JSONObject req = new JSONObject();
        try {
            req.put("id", studio_Id);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        String jsonString = new Gson().toJson(req);
        JSONObject convertedObject = new Gson().fromJson(jsonString, JSONObject.class);

        StudioId si = new StudioId();
        si.id = studio_Id;
        myApiEndpointInterface.getParticularStudio(
                "Bearer " + "142|1VWFImtOUmXl78S9psdBpouZ8TR5UB35h81IjCpH",
                si).enqueue(new Callback<SingleStudioResponse>() {
            @Override
            public void onResponse(Call<SingleStudioResponse> call, retrofit2.Response<SingleStudioResponse> response) {
                pb.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.isSuccessful()) {
                        recyclerviewStudioAdapter = new RecyclerviewStudioAdapter(response.body().data.workout, SelectStudioActivity.this);
                        recyclerView.setAdapter(recyclerviewStudioAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<SingleStudioResponse> call, Throwable t) {
                pb.setVisibility(View.GONE);
                call.cancel();
                Common.toast(SelectStudioActivity.this, t.getLocalizedMessage());
            }
        });
    }
}
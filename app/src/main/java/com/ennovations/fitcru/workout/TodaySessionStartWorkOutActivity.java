package com.ennovations.fitcru.workout;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.TodaySessionStartWorkoutPojo;
import com.ennovations.fitcru.workout.adapter.TodaySessionStartWorkoutAdapter;

import java.util.ArrayList;

public class TodaySessionStartWorkOutActivity extends AppCompatActivity {

    ImageView back_button;
    RecyclerView recyclerView;
    ArrayList<TodaySessionStartWorkoutPojo> todaySessionStartWorkoutList = new ArrayList<>();
    TodaySessionStartWorkoutAdapter todaySessionStartWorkoutAdapter;
    TextView header, bottom_header;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_today_sesson_start_work_out);

        back_button = (ImageView) findViewById(R.id.image_backpressed);
        header = (TextView) findViewById(R.id.text_top);
        bottom_header = (TextView) findViewById(R.id.text_bottom);

        header.setText("Workout Name");
        bottom_header.setText("Note will come here");

        recyclerView = (RecyclerView) findViewById(R.id.rv_workout_reps);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        todaySessionStartWorkoutList.add(new TodaySessionStartWorkoutPojo("0.30", "Full Sit-Ups"));
        todaySessionStartWorkoutList.add(new TodaySessionStartWorkoutPojo("0.50", "High Plank"));
        todaySessionStartWorkoutList.add(new TodaySessionStartWorkoutPojo("2.45", "The Baseline Push"));
        todaySessionStartWorkoutList.add(new TodaySessionStartWorkoutPojo("0.30", "Full Sit-Ups"));
        todaySessionStartWorkoutList.add(new TodaySessionStartWorkoutPojo("0.50", "High Plank"));
        todaySessionStartWorkoutList.add(new TodaySessionStartWorkoutPojo("2.45", "The Baseline Push"));
        todaySessionStartWorkoutList.add(new TodaySessionStartWorkoutPojo("0.30", "Full Sit-Ups"));
        todaySessionStartWorkoutList.add(new TodaySessionStartWorkoutPojo("0.50", "High Plank"));
        todaySessionStartWorkoutList.add(new TodaySessionStartWorkoutPojo("2.45", "The Baseline Push"));
        todaySessionStartWorkoutList.add(new TodaySessionStartWorkoutPojo("0.30", "Full Sit-Ups"));
        todaySessionStartWorkoutList.add(new TodaySessionStartWorkoutPojo("0.50", "High Plank"));
        todaySessionStartWorkoutList.add(new TodaySessionStartWorkoutPojo("2.45", "The Baseline Push"));
        todaySessionStartWorkoutList.add(new TodaySessionStartWorkoutPojo("0.30", "Full Sit-Ups"));
        todaySessionStartWorkoutList.add(new TodaySessionStartWorkoutPojo("0.50", "High Plank"));
        todaySessionStartWorkoutList.add(new TodaySessionStartWorkoutPojo("2.45", "The Baseline Push"));

        todaySessionStartWorkoutAdapter = new TodaySessionStartWorkoutAdapter(todaySessionStartWorkoutList);
        recyclerView.setAdapter(todaySessionStartWorkoutAdapter);

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
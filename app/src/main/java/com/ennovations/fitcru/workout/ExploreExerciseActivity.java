package com.ennovations.fitcru.workout;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.ExploreExercisePojo;
import com.ennovations.fitcru.workout.adapter.RecyclerViewExploreExerciseAdapter;

import java.util.ArrayList;

public class ExploreExerciseActivity extends AppCompatActivity {

    ImageView back_button;
    RecyclerView recyclerView;
    ArrayList<ExploreExercisePojo> exploreExercisePojoArrayList = new ArrayList<>();
    RecyclerViewExploreExerciseAdapter recyclerViewExploreExerciseAdapter;
    TextView save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explore_exercise);

        recyclerView = (RecyclerView) findViewById(R.id.rv_explore_exercise);
        back_button = (ImageView) findViewById(R.id.image_backpressed);
        save = (TextView) findViewById(R.id.btn_save_and_continue);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        exploreExercisePojoArrayList.add(new ExploreExercisePojo(R.drawable.cw1, "15 ,min", "Full Sit-Ups", "Intermediate, No Equip", false));
        exploreExercisePojoArrayList.add(new ExploreExercisePojo(R.drawable.cw2, "15 ,min", "High Plank", "Intermediate, No Equip", false));
        exploreExercisePojoArrayList.add(new ExploreExercisePojo(R.drawable.cw3, "15 ,min", "The Leg Up", "Intermediate, No Equip", false));
        exploreExercisePojoArrayList.add(new ExploreExercisePojo(R.drawable.workout, "15 ,min", "The Baseline Push", "Intermediate, No Equip", false));
        exploreExercisePojoArrayList.add(new ExploreExercisePojo(R.drawable.cw1, "15 ,min", "Full Sit-Ups", "Intermediate, No Equip", false));
        exploreExercisePojoArrayList.add(new ExploreExercisePojo(R.drawable.cw2, "15 ,min", "High Plank", "Intermediate, No Equip", false));
        exploreExercisePojoArrayList.add(new ExploreExercisePojo(R.drawable.cw3, "15 ,min", "The Leg Up", "Intermediate, No Equip", false));
        exploreExercisePojoArrayList.add(new ExploreExercisePojo(R.drawable.workout, "15 ,min", "The Baseline Push", "Intermediate, No Equip", false));
        exploreExercisePojoArrayList.add(new ExploreExercisePojo(R.drawable.cw1, "15 ,min", "Full Sit-Ups", "Intermediate, No Equip", false));
        exploreExercisePojoArrayList.add(new ExploreExercisePojo(R.drawable.cw2, "15 ,min", "High Plank", "Intermediate, No Equip", false));
        exploreExercisePojoArrayList.add(new ExploreExercisePojo(R.drawable.cw3, "15 ,min", "The Leg Up", "Intermediate, No Equip", false));
        exploreExercisePojoArrayList.add(new ExploreExercisePojo(R.drawable.workout, "15 ,min", "The Baseline Push", "Intermediate, No Equip", false));

        recyclerViewExploreExerciseAdapter = new RecyclerViewExploreExerciseAdapter(exploreExercisePojoArrayList);
        recyclerView.setAdapter(recyclerViewExploreExerciseAdapter);

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
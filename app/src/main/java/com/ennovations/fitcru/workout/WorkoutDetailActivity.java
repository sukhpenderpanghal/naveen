package com.ennovations.fitcru.workout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;

import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.WorkoutDetailPojo;
import com.ennovations.fitcru.workout.adapter.RecyclerviewWorkoutDetailAdapter;

import java.util.ArrayList;

public class WorkoutDetailActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<WorkoutDetailPojo> workoutDetailPojoArrayList = new ArrayList<>();
    RecyclerviewWorkoutDetailAdapter recyclerviewWorkoutDetailAdapter;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_detail);

        textView = (TextView) findViewById(R.id.text);
        textView.setText(R.string.lbl_workout);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerviewWorkoutDetailAdapter = new RecyclerviewWorkoutDetailAdapter(workoutDetailPojoArrayList);
        recyclerView.setAdapter(recyclerviewWorkoutDetailAdapter);



    }
}
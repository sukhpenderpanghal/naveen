package com.ennovations.fitcru.workout.challenge;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ennovations.fitcru.Api.ApiClient;
import com.ennovations.fitcru.Api.MyApiEndpointInterface;
import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.ChallengePojo;
import com.ennovations.fitcru.pojo.WorkOutChallengeResponse;
import com.ennovations.fitcru.util.Common;
import com.ennovations.fitcru.workout.adapter.RecyclerViewChallengeAdapter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChallengeFragment extends Fragment {

    RecyclerView recyclerView_current, rv_upcoming;
    RecyclerViewChallengeAdapter recyclerViewChallengeAdapter;
    MyApiEndpointInterface myApiEndpointInterface;
    ProgressBar pb;
    String token;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_challenge, container, false);

        myApiEndpointInterface = ApiClient.getClient().create(MyApiEndpointInterface.class);
        token = Common.getToken(getContext());

        pb = view.findViewById(R.id.progressBarr);
        recyclerView_current = view.findViewById(R.id.rv_current_challenge);
        rv_upcoming = view.findViewById(R.id.rv_upcoming_challenge);
        recyclerView_current.setHasFixedSize(true);
        recyclerView_current.setLayoutManager(new LinearLayoutManager(getContext()));
        rv_upcoming.setHasFixedSize(true);
        rv_upcoming.setLayoutManager(new LinearLayoutManager(getContext()));
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pb.setVisibility(View.VISIBLE);
        callChallengeApi();
    }

    private void callChallengeApi() {
        myApiEndpointInterface.getWorkOutChallenges("Bearer" + "142|1VWFImtOUmXl78S9psdBpouZ8TR5UB35h81IjCpH").enqueue(new Callback<WorkOutChallengeResponse>() {
            @Override
            public void onResponse(Call<WorkOutChallengeResponse> call, Response<WorkOutChallengeResponse> response) {
                WorkOutChallengeResponse workOutChallengeResponse = response.body();

                pb.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.isSuccessful() && response.body().status == 200) {

                        Common.toast(getContext(), response.body().message);
                        if (response.body().challengeData != null) {
                            recyclerViewChallengeAdapter = new RecyclerViewChallengeAdapter(response.body().challengeData.challengeCurrents, getContext());
                            recyclerView_current.setAdapter(recyclerViewChallengeAdapter);
                        }
                    } else {
                        Common.toast(getContext(), response.body().message);
                    }
                } else {
                    Common.toast(getContext(), response.body().message);
                }
            }

            @Override
            public void onFailure(Call<WorkOutChallengeResponse> call, Throwable t) {
                call.cancel();
                pb.setVisibility(View.GONE);
                Common.toast(getContext(), t.getLocalizedMessage());
            }
        });
    }
}
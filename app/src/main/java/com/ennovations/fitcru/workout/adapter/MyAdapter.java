package com.ennovations.fitcru.workout.adapter;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ennovations.fitcru.workout.challenge.ChallengeFragment;
import com.ennovations.fitcru.workout.plan.PlansFragment;
import com.ennovations.fitcru.workout.studio.StudioFragment;
import com.ennovations.fitcru.workout.todaySession.TodaySessionFragment;

public class MyAdapter extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;

    public MyAdapter(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                PlansFragment plansFragment = new PlansFragment();
                return plansFragment;
            case 1:
                StudioFragment studioFragment = new StudioFragment();
                return studioFragment;
            case 2:
                ChallengeFragment challengeFragment = new ChallengeFragment();
                return challengeFragment;
            case 3:
                TodaySessionFragment todaySessionFragment = new TodaySessionFragment();
                return todaySessionFragment;
            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;
    }
}

package com.ennovations.fitcru.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ennovations.fitcru.R;
import com.ennovations.fitcru.pojo.DataClass;

public class HorizonDataAdapter extends RecyclerView.Adapter<HorizonDataAdapter.ViewHolder> {
    private DataClass[] list;

    public HorizonDataAdapter(DataClass[] list) {
    this.list=list;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.horizon_item_list, parent, false);
        HorizonDataAdapter.ViewHolder viewHolder = new HorizonDataAdapter.ViewHolder(listItem);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final DataClass myListData = list[position];
        holder.IV.setImageResource(myListData.getId());
        holder.TV.setText(myListData.getOption());

    }

    @Override
    public int getItemCount() {
        return list.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView IV;
        public TextView TV;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            IV=itemView.findViewById(R.id.horizonIV);
            TV=itemView.findViewById(R.id.horizonTV);
        }
    }
}
